from selenium.webdriver.support.wait import WebDriverWait
import re
import unittest
from selenium import webdriver
import time
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC 
#from Pages.PageLogin import Page_login
import autoit



class Agradecimiento(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")
    
    def test_agradecimiento(self):

        #INSTRUCCIONES PARA ABRIR VENTANA SGI
        
        #URL

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"        
        
        driver = self.driver            
        driver.maximize_window()        
        titulopage = driver.title        
        WebDriverWait(driver, 10).until(EC.title_contains((titulopage)))
        driver.get(url)  
        #WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID,"username"))) 
        #titulo_Sgi = driver.find_element_by_id("userName").text
        #print(titulo_Sgi) 
        time.sleep(10)

        # verify title is SGI             
                
        #assertEqual = self.assertEqual
        #assertEqual(titulo_Sgi,"Usuario", "No pudo acceder a SGI")          

        #DATOS DE USUARIO 

        usuario = "EXT_KaPerez"
        contrasenia = "mpDA4Hw6"
        username_id = "userName"
        password = "password"
        #disable_id = "disable"

        driver.find_element_by_id(username_id).send_keys(usuario)
        #driver.find_element_by_id(password_id).send_keys(contrasena)
        #driver.find_element_by_id(disable_id).click()     

        password_id = WebDriverWait(driver, 10, 0.5).until(
        EC.presence_of_element_located((By.ID, password)))
        password_id.send_keys(contrasenia)
        password_id.submit() 

        time.sleep(10)

        #BUSCAR

        driver.find_element_by_xpath("//a[@id='searchTypeEntity']//span[@class='menu-text']").click()
        time.sleep(8)

        #DATOS DE AFILIADO

        DU = "45822651"

        driver.find_element_by_id("txtAfiliadoDocumento").send_keys(DU)
        driver.find_element_by_id("btnSearch2").click()
        time.sleep(5)

        #MOVER HASTA LA RUEDA

        searchBtn = driver.find_element_by_css_selector("#ace-settings-btn")
        webdriver.ActionChains(driver).click_and_hold(searchBtn).perform()
        time.sleep(3)

        #SELECCIONAR PROCESO

        driver.find_element_by_id("inputProcess").send_keys("AGRA")
        time.sleep(2)
        driver.find_element_by_id("41").click()
        time.sleep(5)

        #ACCEDER AL FRAME

        driver.switch_to.frame(driver.find_element_by_id("my_frame41"))
        time.sleep(10)
        
        #GESTION AGRADECIMIENTO

        driver.find_element_by_id("observacion").send_keys("Agradecimiento")    
        time.sleep(2)

        #GESTION AGRADECIMIENTO MOTIVO  

        driver.find_element_by_id("submotivo").click()
        driver.find_element_by_xpath("//option[@value='10']").click()
        time.sleep(5)
        
        #GRABAR AGRADECIMIENTO

        driver.find_element_by_id("btnSave").click()
        time.sleep(4)   

        #CAPTURA DE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_ETE\\generaciontramite.png")

        time.sleep(5)    

        #TOMAR TRÁMITE
    
        numerotramite = driver.find_element_by_css_selector("div[class='modal-body']").text

        print(numerotramite)

        busqueda_tramite = r"(?<=Trámite )\w+"

        numero_tramite = re.findall(str(busqueda_tramite),numerotramite, re.IGNORECASE)

        print(numero_tramite) 

        tramite = "#" + str(numero_tramite[0])
        
        print(tramite)        
        
        #tramite = "#" + numerotramite[31:40]

        #print("Impresión de"+ tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(5)

        #BUSQUEDA INTELIGENTE DE TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)

        #ADJUNTAR DOCUMENTO

        adjuntar_xpath = "//i[@data-rel='tooltip']"

        driver.find_element_by_xpath(adjuntar_xpath).click()
        time.sleep(2)

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\maty")        

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(10)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)

        #SIGUIENTE A PASO 2

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #GESTION TRAMITE

        lupa =  driver.find_element_by_id("advanceSearch")
        
        mover = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,mover).perform()
        time.sleep(5)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(5)

        #INGRESAR IFRAME TASK
        
        driver.switch_to.frame(driver.find_element_by_id("iframeTask"))
        time.sleep(6)

        #FINALIZAR EL TRAMITE

        driver.find_element_by_id("actions").click()
        time.sleep(2)

        driver.find_element_by_xpath("//option[@id='1']").click()
        time.sleep(2)

        driver.find_element_by_id("btnSaveAndEndTask").click()
        time.sleep(2) 

        #BUSCAR TRAMITE FINALIZADO 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(1) 

        #--------INGRESO AL TRAMITE

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).move_to_element(icono).perform()

        #CAPTURA DE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_ETE\\tramitefinalizado.png")

        time.sleep(5)   

if __name__ == '__main__':
    unittest.main()






