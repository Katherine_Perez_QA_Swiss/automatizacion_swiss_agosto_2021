import unittest
from selenium import webdriver
import time
import autoit
from selenium.webdriver.common.keys import Keys
import datetime
import dateutil


class Gestion(unittest.TestCase):
    
    def setUp(self):
        
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

    def test_autorizaciones(self):

        #INTRUCCIONES PARA INGRESAR EN SGI

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"

        driver = self.driver
        driver.maximize_window()
        driver.get(url2)
        time.sleep(5)

        #INGRESO DE USUARIOS

        usuario = "ext_roperalt"
        contrasena = "Qactions01"
        username = "userName"
        password = "password"
        disable = "disable"

        driver.find_element_by_id(username).send_keys(usuario)
        driver.find_element_by_id(password).send_keys(contrasena)
        driver.find_element_by_id(disable).click()
        time.sleep(4)        
        
        #BUSCAR PROCESO

        buscar = "//a[@id='searchTypeEntity']//span[@class='menu-text']"
        
        driver.find_element_by_xpath(buscar).click()
        time.sleep(10)

        #INGRESO DE DU

        DU = "45822651"
        afiliado = "txtAfiliadoDocumento"
        boton = "btnSearch2"

        driver.find_element_by_id(afiliado).send_keys(DU)
        driver.find_element_by_id(boton).click()

        time.sleep(15)

        #MOVER HASTA LA RUEDA

        buscarueda = driver.find_element_by_css_selector("#ace-settings-btn")
        webdriver.ActionChains(driver).click_and_hold(buscarueda).perform()
        time.sleep(4)

        #SELECCIONAR UN PROCESO

        proceso = "inputProcess"
        nombre = "Autoriz"
        numero = "94"
        
        driver.find_element_by_id(proceso).send_keys(nombre)
        time.sleep(2)
        driver.find_element_by_id(numero).click()
        time.sleep(3)

        #ACCEDER AL IFRAME PROCESO

        iframe = "my_frame94" 

        driver.switch_to.frame(driver.find_element_by_id(iframe))
        time.sleep(10) 

        #GESTION AUTORIZACIONES

        #---------REQUIERE MATERIAL SI

        requierematerial = "//div[@id='requiereMaterial']//input[@value='true']"

        driver.find_element_by_xpath(requierematerial).click()
        time.sleep(1)

        #---------DIAGNOSTICO

        diagnostico = "poseeDiagnostico"

        driver.find_element_by_id(diagnostico).click()
        time.sleep(1)

        #---------SELLO

        sello = "poseeSello"

        driver.find_element_by_id(sello).click()
        time.sleep(1)

        #---------AUTORIZACION

        autorizacion = driver.find_element_by_id("tipoAutorizacion")
        webdriver.ActionChains(driver).move_to_element(autorizacion).perform()
        time.sleep(4)
             
        #---------CIRUGIA

        cirugia = "option[value='C']"

        driver.find_element_by_css_selector(cirugia).click()
        time.sleep(2)

        #---------GESTION

        gestion = "//div[@id='tiposGestion']//input[@value='2']"

        driver.find_element_by_xpath(gestion).click()
        time.sleep(1)

        #---------MEDICACION

        medicacion = "//select[@id='requiereMedicacion']//option[@value='1']"

        driver.find_element_by_xpath(medicacion).click()
        time.sleep(1)

        #---------SELECCIONAR

        seleccionar = "historia-1322190"

        driver.find_element_by_id(seleccionar).click()
        time.sleep(1)

        #---------TRATAMIENTO

        tratamiento = "solicitudAutorizacion[esInicioTratamiento]"

        driver.find_element_by_name(tratamiento).click()
        time.sleep(1)

        #---------SUBMOTIVO

        submotivo = "//select[@id='submotivos']//option[@value='5']"

        driver.find_element_by_xpath(submotivo).click()
        time.sleep(1)

        #---------ATRIBUTO

        atributo = "//select[@id='atributos']//option[@value='4']"

        driver.find_element_by_xpath(atributo).click()
        time.sleep(1)

        #---------PERIODICIDAD

        periodicidad = "//select[@id='periodicidades']//option[@value='17']"

        driver.find_element_by_xpath(periodicidad).click()
        time.sleep(1)

         #---------ADJUNTAR DOCUMENTO

        adjuntar = "i[data-title='Adjuntar archivos']"
        
        driver.find_element_by_css_selector(adjuntar).click()
        
        time.sleep(4)

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\maty")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(10)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)
         

      #------METODO PARA FECHA------------  

        base = datetime.date.today()

        fechahoy = base.strftime("%d/%m/%Y")

        print(fechahoy)

        base_dias = base + datetime.timedelta(days=3)

        fecha_3dias = base_dias.strftime("%d/%m/%Y")

        print(fecha_3dias)

        #---------FECHA DE HOY + 3 DÍAS
             
        fechaorden = driver.find_element_by_id("fechaOrden")
        webdriver.ActionChains(driver).move_to_element(fechaorden).perform()
        time.sleep(4)     
        
        calendario = "//body/div[@id='page-content']/div[@class='row-fluid']/div[@id='content1']/div/div[@id='form-div']/div[@class='span12']/div/div[@class='row-fluid']/div[@class='span12']/div[@class='row-fluid']/div[@class='span12']/div[@class='widget-box']/div[@class='widget-body']/div[@class='widget-main']/div[@class='row-fluid']/div[@id='step-container']/div[@id='step1']/form[@id='gestion-autorizaciones-form1']/div[6]/div[1]"
        driver.find_element_by_xpath(calendario).click()
       

        driver.find_element_by_id("fechaOrden").send_keys(fechahoy)
        time.sleep(3)

        calendario1 = "//body//div[@id='page-content']//div[@id='fechaProcedimientoDiv']//div//div//div[1]//span[1]//i[1]"
        fechaProcedimiento = "fechaProcedimiento"

        
        driver.find_element_by_xpath(calendario1).click()
        driver.find_element_by_id(fechaProcedimiento).send_keys(fecha_3dias)

        time.sleep(3)

        iconohora = "//i[@class='icon-time']"
        hora = "//a[@data-action='incrementHour']//i"

        driver.find_element_by_xpath(iconohora).click()
        driver.find_element_by_xpath(hora).click()

        #---------GESTION

        #---------Provision

        provision = "//body/div[@id='page-content']/div/div[@id='content1']/div/div[@id='form-div']/div/div/div/div/div/div/div/div/div/div/div[@id='step-container']/div[@id='step1']/form[@id='gestion-autorizaciones-form1']/div[12]/div[1]/div[1]/input[1]"
		
        driver.find_element_by_xpath(provision).click()

        #---------Paciente

        paciente = "ambuInter1"

        driver.find_element_by_id(paciente).click()

        #---------Complejidad

        complejidad = "//div[@id='tipoComplejidadDiv']//div//div//input[@value='1']"

        driver.find_element_by_xpath(complejidad).click()

        #---------Lupa

        lupa = "//a[@id='botonBuscarPrestador']//i"
        
        driver.find_element_by_xpath(lupa).click()

        #---------Codigo

        codigo = "//input[@id='codigoBusqueda']"

        driver.find_element_by_xpath(codigo).send_keys("1130")
        
        #---------Buscar1

        buscar1 = "btnBusquedaPrestadorBuscar"

        driver.find_element_by_id(buscar1).click()
        time.sleep(5)

        #---------Codigo1

        codigo1 = "//a[@title='Click para seleccionar']"
        
        driver.find_element_by_xpath(codigo1).click()
        time.sleep(1)

        #---------Calle

        calle = "//a[normalize-space()='LARREA']"
        driver.find_element_by_xpath(calle).click()
        time.sleep(1)

        #---------AVANZAR AL SEGUNDO PASO

        siguiente = "btnContinuar"

        driver.find_element_by_id(siguiente).click()
        time.sleep(3)

        #---------GESTION SEGUNDO PASO

        #---------LUPA

        lupa1 = "botonBuscarProfesionalEfector"

        driver.find_element_by_id(lupa1).click()
        time.sleep(1)

        #---------Apellido

        apellido = "apellidoRazonSocialBusqueda"

        driver.find_element_by_id(apellido).send_keys("Fernandez")
        time.sleep(3)
        
        #---------Busqueda 

        busqueda1 = "btnBusquedaProfesionalEfectorBuscar"

        driver.find_element_by_id(busqueda1).click()
        time.sleep(4)

        #---------CUIT 

        codigo2 = "//a[normalize-space()='70099']"
        driver.find_element_by_xpath(codigo2).click()
        time.sleep(1)

        #---------ORDEN MEDICA

        ordenmedica = "ambosIguales"

        driver.find_element_by_id(ordenmedica).click()
        time.sleep(2)

        #---------AVANZAR AL TERCER PASO

        siguiente1 = "btnContinuar"

        driver.find_element_by_id(siguiente1).click()
        time.sleep(3)

        #---------GESTION TERCER PASO

        #---------Telefono

        telefono = "cargarNuevoTelefonoContacto"

        driver.find_element_by_id(telefono).click()
        time.sleep(2)

        #---------Provincia

        provincia = "prestadorPhoneprov"

        driver.find_element_by_id(provincia).click()
        time.sleep(2)

        #---------Capital

        capital ="//select[@id='prestadorPhoneprov']//option[@value='2']"
		
        driver.find_element_by_xpath(capital).click()
        time.sleep(2)

        #---------Partido

        partido ="prestadorPhonepartido"

        driver.find_element_by_id(partido).click()
        time.sleep(1)

        #---------Capital1

        capital1 = "//select[@id='prestadorPhonepartido']//option[@value='135']"		
        
        driver.find_element_by_xpath(capital1).click()
        time.sleep(1)

        #---------Localidad  		
		
        localidad = "prestadorPhoneloc"
		
        driver.find_element_by_id(localidad).click()
        time.sleep(1)

        #---------Caba
         
        caba = "//select[@id='prestadorPhoneloc']//option[@value='2102']"
		
        driver.find_element_by_xpath(caba).click()
        time.sleep(1)
        
        #---------CODIGO POSTAL

        cp = "prestadorPhonecp"

        driver.find_element_by_id(cp).send_keys("1224")
        time.sleep(1)

        #---------CODIGO POSTAL

        prefijo = "prestadorPhoneprefijo"

        driver.find_element_by_id(prefijo).send_keys("011")
        time.sleep(1)

        #---------NUMERO		
		
        numero ="prestadorPhonenumero"

        driver.find_element_by_id(numero).send_keys("24028799")
        time.sleep(1)

        #---------VALIDAR
	
        validar = "prestadorPhonevalidatePhoneButton"

        driver.find_element_by_id(validar).click()
        time.sleep(1)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.ENTER)

        time.sleep(1) 

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(3)

        #---------GENERACIÓN TRAMITE
        
        finalizar = "btnContinuar"

        driver.find_element_by_id(finalizar).click()
        time.sleep(20)

        #---------CAPTURA DE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_03 GESTIÓN DE AUTORIZACIONES\\Evidencia_ETE\\gestión_autorizacion.png")
        time.sleep(3) 

        #--------TOMAR TRAMITE

        numerotramite = driver.find_element_by_css_selector("div[class='bootbox modal fade in']>div[class='modal-body']").text

        tramite = "#" + numerotramite[31:40]

        print("Tramite numero" + tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

              
        #--------Gestion Autorizaciones - Completar Precarga

        #--------SIGUIENTE A PASO 2

        continuar = "btnContinuar"

        driver.find_element_by_id(continuar).click()
        time.sleep(1)

        #--------TIPO DE AGRUPACIÓN------PASO 2

        agrupacion = driver.find_element_by_id("tiposAgrupacion")
        webdriver.ActionChains(driver).click_and_hold(agrupacion).perform()
        time.sleep(4)

        #--------TIPO DE AGRUPACIÓN/CARDIOLOGIA------PASO 2

        cardiologia = "//option[normalize-space()='Cardiología']"

        driver.find_element_by_xpath(cardiologia).click()
        time.sleep(1)

         #--------SIGUIENTE A PASO 3

        continuar2 = "btnContinuar"

        driver.find_element_by_id(continuar2).click()
        time.sleep(1)

         #--------Accion--------PASO 3

        accion = "actions"

        driver.find_element_by_id(accion).click()     
        
        obrasocial = "5"

        driver.find_element_by_id(obrasocial).click() 

       	 #--------SALVAR TAREA CON OBRA SOCIAL
		
        continuar3 = "//span[@id='btnSaveAndEndTask']"
		
        driver.find_element_by_xpath(continuar3).click() 

        time.sleep(10)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

         #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #--------Gestion Autorizaciones - Controlar Obra Social

        #GENERAR AUTORIZACION

        obrasocial2 = "coberturaObraSocial"

        driver.find_element_by_id(obrasocial2).click()

        time.sleep(1)	

         #--------GENERAR OBRA SOCIAL
		
        obra = "//option[normalize-space()='Obra social']"

        driver.find_element_by_xpath(obra).click()

        time.sleep(1)	

        #--------OBSERVACIÓN

        observacion = "solicitudAutorizacion[observacionObraSocial]"

        driver.find_element_by_name(observacion).send_keys("Prueba Generacion Autorizaciones")

        time.sleep(3)

        #--------GENERAR AUTORIZACIÓN

        generar = "//option[@value='Generar_Autorizacion']"

        driver.find_element_by_xpath(generar).click()

        time.sleep(10)

        #--------SALVAR TAREA CON OBRA SOCIAL
		
        continuar4 = "//span[@id='btnSaveAndEndTask']//i"
		
        driver.find_element_by_xpath(continuar4).click() 

        time.sleep(10)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-pencil']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #-------- Gestion Autorizaciones - Generar Autorizacion

        #---------DIAS DE TERAPIA

        diasinternacion = "diasInternacion"

        driver.find_element_by_id(diasinternacion).send_keys("2")

        time.sleep(2)

        #---------BOTON OPENDAY

        btnOpenDay = "btnOpenDaysPopup"

        driver.find_element_by_id(btnOpenDay).click()

        time.sleep(2)    

        #---------DIAS DE INTERNACION

        diasTerapia = "solicitudAutorizacion[diasPorSala]1"

        driver.find_element_by_name(diasTerapia).send_keys("2")

        time.sleep(2)
            
        #---------ACEPTAR DIA

        btnAceptarDay = "btnAceptarDiasInternacion"

        driver.find_element_by_id(btnAceptarDay).click()

        time.sleep(2)

        #---------BOTON BUSQUEDA PRESTADOR
        
        btnBusqPrest = "btnShowModalPrestacionSgi"

        driver.find_element_by_id(btnBusqPrest).click()

        time.sleep(2)

        #---------BOTON BUSQUEDA PRESTADOR

        sanatorio = "//body/div/div/a[2]" 

        driver.find_element_by_xpath(sanatorio).click()

        time.sleep(2)            
        
        #---------CODIGO BUSQUEDA PRESTADOR
        
        codigoBusqPrest = "prestacionCodigo"

        driver.find_element_by_id(codigoBusqPrest).send_keys("01050713")

        time.sleep(2)

        #---------CODIGO BUSQUEDA PRESTACION
        
        btnBusqPrest2 = "btnBuscarPrestacion"

        driver.find_element_by_id(btnBusqPrest2).click()

        time.sleep(10)

        #---------ESTADO PRESTACION
        
        sgiEstado = "//tbody//tr//td//select//option[@value='A']"

        driver.find_element_by_xpath(sgiEstado).click()

        time.sleep(3)

        #---------FINALIZAR

        sgiAcciones =  "//option[@value='Finalizar']"

        driver.find_element_by_xpath(sgiAcciones).click()

        time.sleep(3)

        #---------BOTON FINALIZAR
        
        btnFinalizar4 = "//span[@id='btnSaveAndEndTask']"

        driver.find_element_by_xpath(btnFinalizar4).click()

        time.sleep(5)       

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_03 GESTIÓN DE AUTORIZACIONES\\Evidencia_ETE\\generacionautorizacion.png")
        time.sleep(5)  

if __name__ == '__main__':
    unittest.main()
    