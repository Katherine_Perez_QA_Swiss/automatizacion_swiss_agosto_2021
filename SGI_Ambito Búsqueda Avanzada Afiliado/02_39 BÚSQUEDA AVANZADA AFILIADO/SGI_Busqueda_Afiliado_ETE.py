import unittest
from unittest import main
from selenium import webdriver
import time

class Busqueda_Afiliado(unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

    def test_Busqueda(self):

        #ACCESO A SGI

        driver = self.driver
        driver.maximize_window()

        url = "http://sgipre/sgi-app/#login"
        url2= "http://sgiqa/sgi-app/#login"

        driver.get(url)
        time.sleep(3)

        #VERIFICAR ACCESO A SGI 

        assertEqual = self.assertEqual

        titulopage =  driver.title

        assertEqual = ("SGI", titulopage, "No se logró acceder a SGI")

        #DATOS DE ACCESO SGI

        usuario = "ext_roperalt"
        contrasena = "Qactions01"
        user_id = "userName"
        password_id = "password"
        ingresar_button = "disable"

        driver.find_element_by_id(user_id).send_keys(usuario)
        driver.find_element_by_id(password_id).send_keys(contrasena)
        driver.find_element_by_id(ingresar_button).click()
        time.sleep(3)

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "45822651"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_39 BÚSQUEDA AVANZADA AFILIADO\\Evidencia_ETE\\busqueda_afiliado.png")
        time.sleep(3) 

if __name__ == '__main__':
    unittest.main()
    



