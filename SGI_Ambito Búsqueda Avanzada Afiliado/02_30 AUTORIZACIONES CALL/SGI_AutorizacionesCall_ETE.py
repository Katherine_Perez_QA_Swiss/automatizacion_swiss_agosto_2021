import unittest
from selenium import webdriver
import time

class Autorizaciones(unittest.TestCase):

    def setUp(self):
        
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

    def test_autorizaciones(self):

        #INTRUCCIONES PARA INGRESAR EN SGI

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"

        driver = self.driver
        driver.maximize_window()
        driver.get(url2)
        time.sleep(5)

        #INGRESO DE USUARIOS

        usuario = "ext_roperalt"
        contrasena = "Qactions01"

        driver.find_element_by_id("userName").send_keys(usuario)
        driver.find_element_by_id("password").send_keys(contrasena)
        driver.find_element_by_id("disable").click()
        time.sleep(4)

        #BUSCAR PROCESO

        driver.find_element_by_xpath("//a[@id='searchTypeEntity']//span[@class='menu-text']").click()
        time.sleep(10)

        #INGRESO DE DU

        DU = "45822651"

        driver.find_element_by_id("txtAfiliadoDocumento").send_keys(DU)
        driver.find_element_by_id("btnSearch2").click()

        time.sleep(6)

        #MOVER HASTA LA RUEDA

        buscarueda = driver.find_element_by_css_selector("#ace-settings-btn")
        webdriver.ActionChains(driver).click_and_hold(buscarueda).perform()
        time.sleep(4)

        #SELECCIONAR UN PROCESO

        driver.find_element_by_id("inputProcess").send_keys("Call")
        time.sleep(2)
        driver.find_element_by_id("61").click()
        time.sleep(3)

        # ACCEDER AL IFRAME PROCESO

        driver.switch_to.frame(driver.find_element_by_id("my_frame61"))
        time.sleep(3)                  
        
        #GESTIONAR PROCESO CALL

        driver.find_element_by_id("observacion").send_keys("Autorizaciones Call")
        time.sleep(6)

        #GRABAR PROCESO

        driver.find_element_by_id("btnSave").click()
        time.sleep(5)

        #CAPTURA DE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_30 AUTORIZACIONES CALL\\Evidencia_ETE\\tramitecall.png")
        time.sleep(3)       
        
        #TOMAR TRAMITE

        numerotramite = driver.find_element_by_css_selector("div[class='modal-body']").text

        tramite = "#" + numerotramite[31:40]

        print("Tramite numero" + tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)

        #BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)     

        #INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #GESTIONAR TRAMITE CALL

        driver.find_element_by_id("observacion").send_keys("Prueba Autorizaciones Call")
        time.sleep(2)

        #FINALIZAR TRAMITE CALL

        driver.find_element_by_id("actions").click()
        time.sleep(1)
        driver.find_element_by_id("1").click()
        time.sleep(1)
        driver.find_element_by_xpath("//i[@class='icon-arrow-right icon-on-right']").click()

        time.sleep(2)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(3)  

         #--------INGRESO AL TRAMITE

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).move_to_element(icono).perform()

        #-------CAPTURA DE PANTALLA  

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_30 AUTORIZACIONES CALL\\Evidencia_ETE\\CierreAutorizacionesCall.png")

        time.sleep(5)           


if __name__ == '__main__':
    unittest.main()
    
    




