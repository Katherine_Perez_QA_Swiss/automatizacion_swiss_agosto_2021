import unittest
from unittest.main import main
from selenium import webdriver
import time
import autoit
import selenium
from selenium.webdriver.common.keys import Keys
import datetime
import selenium


class DerivacionMedica (unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

         #ACCESO A SGI 

        driver = self.driver

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"

        driver.maximize_window()
        driver.get(url2)
        selenium.webdriver.support.expected_conditions.url_contains(url2, 15)

        #DATOS DE USUARIO 

        usuario = "EXT_MaFernan"
        contrasena = "u2y9fXxch"
        username_id = "userName"
        password_id = "password"
        disable_id = "disable"

        driver.find_element_by_id(username_id).send_keys(usuario)
        driver.find_element_by_id(password_id).send_keys(contrasena)
        driver.find_element_by_id(disable_id).click()

        time.sleep(15)

        #BUSCAR

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(2)

        #INGRESO DE DU

        DU = "29197733"
        afiliado = "txtAfiliadoDocumento"
        boton = "btnSearch2"

        driver.find_element_by_id(afiliado).send_keys(DU)
        driver.find_element_by_id(boton).click()

        time.sleep(15)

        #MOVER HASTA LA RUEDA

        rueda_ccsselector = "#ace-settings-btn"

        buscarueda = driver.find_element_by_css_selector(rueda_ccsselector)
        webdriver.ActionChains(driver).click_and_hold(buscarueda).perform()
        time.sleep(4)

        #SELECCIONAR UN PROCESO

        proceso_id = "inputProcess"
        nombre = "Deri"
        numero_id = "109"
        
        driver.find_element_by_id(proceso_id).send_keys(nombre)
        time.sleep(2)
        driver.find_element_by_id(numero_id).click()
        time.sleep(3)

        #ACCEDER AL IFRAME PROCESO

        iframe = "my_frame109" 

        driver.switch_to.frame(driver.find_element_by_id(iframe))
        time.sleep(5) 
    
    def test01_Derivacion(self):
        
        driver = self.driver

        #GESTION DE PROCESO

        #SWICHT ¿Se verificó la existencia de otro prestador?

        prestador_id = "otroPrestador"

        driver.find_element_by_id(prestador_id).click()

        #SWICHT ¿Se controló el cuadro de convenio de OOSS?

        convenio_id = "convenioOOSS"

        driver.find_element_by_id(convenio_id).click()
        time.sleep(4)

        #LISTA DESPLEGABLE : ¿Cuál es el motivo de ingreso?

        motivo_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/select/option[2]"

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(5)

        #LISTA DESPLEGABLE : Tipo de Servicio----OPCION ATENCION DEMANDA

        servicio_xpath = "//div//div//div//div//div//div//div//div//form[@method='get']//div//div//div//div//div//option[@value='8']"
        
        driver.find_element_by_xpath(servicio_xpath).click()
        time.sleep(15)

        #LISTA DESPLEGABLE : Especialidad

        especialidad_id = "6"
        driver.find_element_by_id(especialidad_id).click()
        time.sleep(5)

        #LUPA DIAGNOSTICO

        lupa_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//a[1]//i[1]"
        driver.find_element_by_xpath(lupa_xpath).click()
        time.sleep(2)

        #POP UP BUSQUEDA AVANZADA DIAGNOSTICOS---CODIGO

        codigo_id = "icdCodigoSearchsolicitudDMP"

        driver.find_element_by_id(codigo_id).send_keys("R05")

        #POP UP BUSQUEDA AVANZADA DIAGNOSTICOS---BUSCAR

        buscar_button_id = "solicitudDMPbtnSearch"

        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(5)

        #Presenta documentación-----Resumen de historia clínica

        resumen_id = "documento-0"

        driver.find_element_by_id(resumen_id).click()

        time.sleep(3)

        #ADJUNTAR DOCUMENTO

        adjuntar_xpath = "//i[@data-rel='tooltip']"

        driver.find_element_by_xpath(adjuntar_xpath).click()
        time.sleep(2)

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\maty")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(10)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)

        #SIGUIENTE A PASO 2

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #SIGUIENTE A PASO 3

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #PASO 3

        #Nombre del Contacto

        nombre_name = "solicitudDMP[contacto][nombre]"

        driver.find_element_by_name(nombre_name).send_keys("Prueba")
        time.sleep(3)

        #Teléfono del Contacto

        telefono_button_id = "cargarNuevoTelefonoContacto"

        driver.find_element_by_id(telefono_button_id).click()
        time.sleep(5)

        #POP UP Nuevo Teléfono---PROVINCIA

        provincia_xpath = "//body/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div/div/select/option[2]"

        driver.find_element_by_xpath(provincia_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---PARTIDO

        partido_xpath = "//body/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div[2]/div[1]/select[1]/option[2]"

        driver.find_element_by_xpath(partido_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---LOCALIDAD

        localidad_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div[3]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(localidad_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---NUMERO

        numero_id2= "prestadorPhonenumero"

        driver.find_element_by_id(numero_id2).send_keys("1123938926")
        time.sleep(2)

        #POP UP Nuevo Teléfono---VALIDAR

        validar_id = "prestadorPhonevalidatePhoneButton"

        driver.find_element_by_id(validar_id).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---CONFIRMAR        
        
        action = webdriver.ActionChains(driver)
        
        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)        

        #FINALIZAR TRAMITE

        finalizar_id = "btnContinuar"

        driver.find_element_by_id(finalizar_id).click()
        time.sleep(3)

        #FORMATO DE FECHA 

        formato = "%d %m %Y %H:%M:%S %Y"

        today = datetime.datetime.today()
        print('ISO     :', today)

        s = today.strftime(formato)
        print('strftime:', s)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\1_generaciontramite.png")
        time.sleep(5)  

        #--------TOMAR TRAMITE

        numerotramite = driver.find_element_by_css_selector("div[class='bootbox modal fade in']>div[class='modal-body']").text

        tramite = "#" + numerotramite[31:40]

        print("Tramite numero" + tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)

        #--------BUSQUEDA INTELIGENTE TRAMITE        

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(2)

        driver.find_element_by_xpath("//input[@aria-controls='listUnassignTaskGrid']").send_keys(tramite)

        time.sleep(2)        

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\2_contactarempresa.png")
        time.sleep(5)                 

        #ABRIR TRAMITE

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(8)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #-------- Derivacion Medica Programada - Contactar Empresa-----CAMPO OBSERVACION

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("prueba")
        time.sleep(2)

        #-------- Derivacion Medica Programada - Contactar Empresa-----OPCION SWISS MEDICAL

        swiss_id = "2"

        driver.find_element_by_id(swiss_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Contactar Empresa-----AVANZAR AUDITAR

        flecha_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath).click()
        time.sleep(10)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(2)       
        

        #--------VALIDACIÓN DE NUMERO DE TRAMITE POR ABRIR 

        #tarea_xpath = driver.find_element_by_xpath("//tbody//tr//td//small//b").text

        #tramite2 = tarea_xpath[0:10]

        #print("Validación" + tramite2)

        #--------BORRAR FILTRO Y COLOCAR TRAMITE NUEVAMENTE

        icono_clear_xpath = "//img[@src='/static/images/edit-clear.png']"

        driver.find_element_by_xpath(icono_clear_xpath).click()

        time.sleep(3)

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)        

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")
        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")    
      

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE

        #if(tramite==tramite2):
            #time.sleep(5) 
            #webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
            #time.sleep(5)            
            #CAPTURE PANTALLA
            #driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\asignarauditor.png")      
            #time.sleep(5)
            #driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
            #time.sleep(8)
            #print("asignarauditor")
            
        #else:
            #driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)
            #time.sleep(5)
            #driver.find_element_by_id("advanceSearch").click()
            #time.sleep(5)
            #webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
            #time.sleep(3)
            #CAPTURE PANTALLA
            #driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\asignarauditor.png")
            #time.sleep(5)
            #driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
            #time.sleep(8)
            #print("AsignarAuditorElse") 
            
        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\3_AsignarAuditor.png")
        time.sleep(5)                 

        #ABRIR TRAMITE

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(8)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #--------- Derivacion Medica Programada - Asignar Auditor

        auditor_xpath = "//option[@value='CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar']"

        observacion_id = "observacion"

        driver.find_element_by_xpath(auditor_xpath).click()
        time.sleep(2)

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Asignar Auditor")
        time.sleep(2)

        #--------- Derivacion Medica Programada - Asignar Auditor---ACCION

        accion_id = "1"
        finalizar_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_id(accion_id).click()
        time.sleep(2)
        driver.find_element_by_xpath(finalizar_xpath).click()
        time.sleep(5)

        #--------BUSQUEDA INTELIGENTE TRAMITE     

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(6)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)     

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(6)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)         

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")
        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")               

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE
        
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()

        time.sleep(5) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\4_AuditarMedicamente.png")      
        
        time.sleep(5)

        driver.find_element_by_xpath("//a[@data-original-title='Abrir']//i").click()

        time.sleep(8)  
        
        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)
        
        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 1

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 2

        #Derivación Regional (de 50 a 150 km)

        derivacion_name = "solicitudDMP[tipoDerivacion]"

        driver.find_element_by_name(derivacion_name).click()
        time.sleep(2)   

        #Tipo Traslado---AEREO

        aereo_xpath = "//input[@value='A']"
        driver.find_element_by_xpath(aereo_xpath).click()
        time.sleep(2)

        #Cantidad de Días

        cantidad_id = "cantidadDias"

        driver.find_element_by_id(cantidad_id).send_keys("1")
        time.sleep(5)

        #Diagnóstico

        diagnostico_xpath = "//body//div//div[@method='get']//div//div//div//div//a[1]//i[1]"

        driver.find_element_by_xpath(diagnostico_xpath).click()
        time.sleep(5)

        #POP UP BUSQUEDA AVANZADA DIAGNOSTICOS---CODIGO

        codigo_id = "icdCodigoSearchsolicitudDMP"

        driver.find_element_by_id(codigo_id).send_keys("k08.8")

        #POP UP BUSQUEDA AVANZADA DIAGNOSTICOS---BUSCAR

        buscar_button_id = "solicitudDMPbtnSearch"

        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(8)

        #Observacion
        
        driver.find_element_by_xpath("//div[@method='get']//div[1]//div[1]//textarea[1]").send_keys("Derivacion Medica Programada - Auditar Medicamente")

        time.sleep(5)        

       
        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 2

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()
        time.sleep(5)
        
        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 3

        #Prestaciones Aprobadas------Descripción

        descripcion_id = "prestacionDescripcionA"

        driver.find_element_by_id(descripcion_id).send_keys("odontologia")
        time.sleep(5)

        #Prestaciones Aprobadas------Descripción---LUPA

        lupa_id2 = "searchAprobadas"

        driver.find_element_by_id(lupa_id2).click()
        time.sleep(10)

        #Prestaciones Aprobadas------Descripción---LUPA-----Busqueda de Prestaciones

        prestacion_xpath = "//a[normalize-space()='993503']"

        driver.find_element_by_xpath(prestacion_xpath).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Auditar Medicamente----CONTINUAR

        continuar_id = "1"

        driver.find_element_by_id(continuar_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Auditar Medicamente----FLECHA

        flecha_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath).click()
        time.sleep(8)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  


        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']") 

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE       
             
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(5)  

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\5_GestionarTurno.png")      
        time.sleep(5)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(8)   
        
        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #Derivacion Medica Programada - Gestionar Turno----OBSERVACION

        observacion_dmp = "observacionDMP"

        driver.find_element_by_id(observacion_dmp).send_keys("Derivacion Medica Programada - Gestionar Turno")
        time.sleep(5)

        #Derivacion Medica Programada - Gestionar Turno-----AGREGAR PRESTADOR

        lupa_prestador_id = "btnBuscarPrestador"

        driver.find_element_by_id(lupa_prestador_id).click()
        time.sleep(2)

        #Derivacion Medica Programada - Gestionar Turno-----POP UP BUSCAR PRESTADOR 

        codigo_id2 = "codigoBusqueda" #1130

        driver.find_element_by_id(codigo_id2).send_keys("1130")
        time.sleep(2)

        #Derivacion Medica Programada - Gestionar Turno-----POP UP LUPA BUSCAR 

        lupa_prestador_id2 = "btnBusquedaPrestadorBuscar"

        driver.find_element_by_id(lupa_prestador_id2).click()
        time.sleep(2)

        #Derivacion Medica Programada - Gestionar Turno-----POP UP SELECCIONAR CODIGO

        codigo_xpath = "//a[@title='Click para seleccionar']"

        driver.find_element_by_xpath(codigo_xpath).click()
        time.sleep(2)

        #Derivacion Medica Programada - Gestionar Turno-----POP UP SELECCIONAR LUGAR DE ATENCION

        calle_xpath = "//a[normalize-space()='TTE GRAL JUAN D PERON']"

        driver.find_element_by_xpath(calle_xpath).click()
        time.sleep(2)

        #Derivacion Medica Programada - Gestionar Turno-----SWICHT SELECCIONE PRESTADOR

        prestador_swith_id = "chkSelectedPrestador"

        driver.find_element_by_id(prestador_swith_id).click()
        time.sleep(4)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 1

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 2----FECHA

        #------METODO PARA FECHA------------  

        base = datetime.date.today()

        fechahoy = base.strftime("%d/%m/%Y")

        #Fecha y Horario del Turno 

        fecha_id = "fechaTurno1"
        hora_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/span[1]/i[1]"
        flecha_hora_xpath = "//a[@data-action='incrementHour']"

        driver.find_element_by_id(fecha_id).send_keys(fechahoy)
        time.sleep(2)

        driver.find_element_by_xpath(hora_xpath).click()
        driver.find_element_by_xpath(flecha_hora_xpath).click()
        time.sleep(2)

        #BOTON AGREGAR FECHA Y HORARIO 

        mas_xpath = "//form[@method='get']//div//div//div//div//div//div//div//div//div//div//div//div//div//button[@onclick='return false;']"

        driver.find_element_by_xpath(mas_xpath).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 2

        finalizar_id = "btnContinuar"

        driver.find_element_by_id(finalizar_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 3

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys(" Derivacion Medica Programada - Gestionar Turno")
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno ----CONTINUAR

        continuar_id = "1"

        driver.find_element_by_id(continuar_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno----FLECHA

        flecha_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath).click()
        time.sleep(2)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        #--------BORRAR FILTRO Y COLOCAR TRAMITE NUEVAMENTE

        icono_clear_xpath = "//img[@src='/static/images/edit-clear.png']"

        driver.find_element_by_xpath(icono_clear_xpath).click()

        time.sleep(3)

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)        

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_xpath("//i[@data-placement='left']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE
                  
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()

        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\6_GestionarFirma.png")
        time.sleep(5) 

        driver.find_element_by_xpath("//a[@data-original-title='Tomar y Abrir']//i").click()
        time.sleep(3)    

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #--------Derivacion Medica Programada - Gestionar Firma Formulario

        #NOMBRE -- APELLIDO ---

        nombre_id = "firmarFormNombre"

        driver.find_element_by_id(nombre_id).send_keys("Prueba")

        time.sleep(2)

        apellido_id = "firmarFormApellido"

        driver.find_element_by_id(apellido_id).send_keys("Prueba2")

        time.sleep(2)

        agregar_id = "btnAgregarTelFirmarForm"

        driver.find_element_by_id(agregar_id).click()

        time.sleep(2)
        
        #POP UP Nuevo Teléfono---PROVINCIA

        provincia_xpath = "//form[@method='get']//div//div//div//div//div//div//div//div//option[@value='1']"

        driver.find_element_by_xpath(provincia_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---PARTIDO

        partido_xpath = "//form[@method='get']//div//div[2]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(partido_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---LOCALIDAD

        localidad_xpath = "//form[@method='get']//div//div//div//div//div//div[3]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(localidad_xpath).click()
        time.sleep(5)

        #POP UP Nuevo Teléfono---NUMERO

        numero_id2= "firmarFormularioPhonenumero"

        driver.find_element_by_id(numero_id2).send_keys("1123938926")
        time.sleep(5)

        #POP UP Nuevo Teléfono---VALIDAR

        validar_id = "firmarFormularioPhonevalidatePhoneButton"

        driver.find_element_by_id(validar_id).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---CONFIRMAR        
        
        confirmar_id = "phoneConfirmButton"

        driver.find_element_by_id(confirmar_id).click()
        time.sleep(2)
        
        #Email

        email_xpath = "//option[@value='otro']"  

        driver.find_element_by_xpath(email_xpath).send_keys("betoknoll@hotmail.com")

        time.sleep(5)

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----Observacion

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys(" Derivacion Medica Programada - Gestionar Firma")

        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----

        #DERIVAR CONTACTAR SOCIO

        socio_id = "2"

        driver.find_element_by_id(socio_id).click()

        time.sleep(5)

        #DERIVAR CONTACTAR SOCIO---FLECHA

        flecha_xpath2 = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath2).click()

        time.sleep(2)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5) 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)                     

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE       
            
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\7_ContactarSocio.png")
        time.sleep(5)

        driver.find_element_by_xpath("//a[@data-original-title='Tomar y Abrir']//i").click()
        time.sleep(3)         

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #Derivacion Medica Programada - Contactar Socio

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Contactar Socio")

        time.sleep(2)

        #FINALIZAR TRAMITE 

        finalizar_id = "1"

        driver.find_element_by_id(finalizar_id).click()

        time.sleep(2)

        #DERIVAR CONTACTAR SOCIO---FLECHA

        flecha_xpath3 = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath3).click()

        time.sleep(5)

         #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)     

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE3\\8_cierretramite.png")
        time.sleep(5)  


if __name__ == '__main__':
    unittest.main()