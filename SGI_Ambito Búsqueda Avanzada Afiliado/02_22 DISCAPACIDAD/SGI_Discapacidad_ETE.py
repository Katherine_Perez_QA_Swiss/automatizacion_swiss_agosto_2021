import unittest
from selenium import webdriver
import time

class Discapacidad(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")
    
    def test_discapacidad(self):

        #INSTRUCCIONES PARA ABRIR VENTANA SGI
        
        #URL

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"        
        
        driver = self.driver
        driver.maximize_window()
        time.sleep(4)   
        driver.get(url2)
        time.sleep(6)

        #INGRESAR CON USUARIO

        usuario="ext_roperalt"
        contrasena="Qactions01"

        driver.find_element_by_id("userName").send_keys(usuario)
        driver.find_element_by_id("password").send_keys(contrasena)
        driver.find_element_by_id("disable").click()
        time.sleep(6)

        #BUSCAR 

        driver.find_element_by_id("searchTypeEntity").click()
        time.sleep(8)

        #INGRESO DATOS DU

        DU = "45822651"

        driver.find_element_by_id("txtAfiliadoDocumento").send_keys(DU)
        time.sleep(2)
        driver.find_element_by_id("btnSearch2").click()
        time.sleep(7)

        #MOVER A LA RUEDA

        mover = driver.find_element_by_id("ace-settings-btn")

        webdriver.ActionChains(driver).click_and_hold(mover).perform()

        time.sleep(8)

        #SELECCIONAR PROCESO

        driver.find_element_by_id("inputProcess").send_keys("DISCA")
        time.sleep(2)
        driver.find_element_by_id("79").click()
        time.sleep(10)

        #INGRESO AL IFRAME

        driver.switch_to_frame(driver.find_element_by_id("my_frame79"))
        time.sleep(2)

        #GESTION PROCESO DISCAPACIDAD

        driver.find_element_by_id("observacion").send_keys("Discapacidad")
        time.sleep(2)

        #GRABAR TRAMITE

        driver.find_element_by_id("btnSave").click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_22 DISCAPACIDAD\\Evidencia_ETE\\tramitediscapacidad.png")
        time.sleep(2)         

        #TOMAR TRAMITE

        numerotramite = driver.find_element_by_css_selector("div[class='modal-body']").text

        tramite = "#" + numerotramite [31:40]

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(3)

        #BUSQUEDA INTELIGENTE TRAMITE

        campo = driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        lupa = driver.find_element_by_id("advanceSearch").click()

        time.sleep(2)

        #INGRESO AL TRAMITE

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()

        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()

        time.sleep(3)

        #INGRESO IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(2)    
        
        #GESTION TRAMITE 

        driver.find_element_by_id("observacion").send_keys("Prueba Cuidados Domiciliarios")

        time.sleep(2)

        #FINALIZAR TRAMITE 

        driver.find_element_by_id("actions").click()
        driver.find_element_by_id("1").click()
        driver.find_element_by_xpath("//i[@class='icon-arrow-right icon-on-right']").click()

        time.sleep(3)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(3)  

         #--------INGRESO AL TRAMITE

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).move_to_element(icono).perform()

        #-------CAPTURA DE PANTALLA  

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_22 DISCAPACIDAD\\Evidencia_ETE\\CierreDiscapacidad.png")

        time.sleep(5)           

if __name__ == '__main__':
    unittest.main()