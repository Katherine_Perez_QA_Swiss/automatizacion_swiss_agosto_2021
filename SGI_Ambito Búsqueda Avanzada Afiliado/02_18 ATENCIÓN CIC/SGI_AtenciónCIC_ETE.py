import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
import time 


class Atencion_CIC(unittest.TestCase):

    def setUp(self):
        
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

        driver = self.driver
        driver.maximize_window()
        time.sleep(1)
        driver.get("http://sgipre/sgi-app/#login")
        time.sleep(4)

        #--------------INGRESO DE DATOS------------------------
        
        driver.find_element_by_id("userName").send_keys("ext_roperalt")
        driver.find_element_by_id("password").send_keys("Qactions01")
        driver.find_element_by_id("disable").click()
        time.sleep(6)

        #--------------DAR CLIC EN BUSCAR------------------------
        
        driver.find_element_by_xpath("//a[@id='searchTypeEntity']//span[@class='menu-text']").click()
        time.sleep(4)

        #--------------INGRESAR DATOS DEL AFILIADO------------------------

        driver.find_element_by_id("txtAfiliadoDocumento").send_keys("29197733")
        driver.find_element_by_id("btnSearch2").click()
        time.sleep(5)

        #--------------MOVER HASTA LA RUEDA------------------------
                
        searchBtn = driver.find_element_by_css_selector("#ace-settings-btn")
        webdriver.ActionChains(driver).click_and_hold(searchBtn).perform()
        time.sleep(3)
        #--------------SELECCIONAR PROCESO------------------------

        driver.find_element_by_id("inputProcess").send_keys("CIC")
        time.sleep(2)
        driver.find_element_by_id("515").click()
        time.sleep(5)

        #------ACCEDER AL FRAME------------

        driver.switch_to.frame(driver.find_element_by_id("my_frame515"))
        time.sleep(2)           

    def test01_resuelto(self):  

        driver = self.driver

        #--------------ACCEDER AL PROCESO------------------------
        
        driver.find_element_by_name("atencionCIC[detalle][motivo][id]").click()
        driver.find_element_by_css_selector("option[value='21']").click()

        driver.find_element_by_name("atencionCIC[detalle][observacion]").send_keys("Proceso CIC")

        time.sleep(5)
         
    #------GRABAR EL PROCESO------------

        driver.find_element_by_id("btnSave").click()
        time.sleep(3)
    
    #------CAPTURA DE PANTALLA------------

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_ETE\\tramiteresolverCIC.png")

        time.sleep(5)      

    #--------TOMAR TRAMITE        

        numerotramite = driver.find_element_by_css_selector("div[class='bootbox modal fade in']>div[class='modal-body']").text

        tramite = "#" + numerotramite[31:40]

        print("Tramite numero" + tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)               

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))
        time.sleep(3)

        #--------GESTION  Atencion CIC - Resolver

        observacionescic = "atencionCIC[detalle][observacionResolver]"

        driver.find_element_by_name(observacionescic).send_keys("Atencion CIC - Resolver")

        time.sleep(3)
        
        #--------ACCION ----RESUELTA

        resuelta = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/select/option[2]"

        driver.find_element_by_xpath(resuelta).click()
        time.sleep(3)

        flecha = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha).click()
        time.sleep(3)

        #--------BUSCAR TRAMITE RESUELTO

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(3)  

        #-------CAPTURA DE PANTALLA RESUELTA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_ETE\\resueltatramiteCIC.png")

        time.sleep(5)

    def test02_consultarechazada(self):  

        driver = self.driver

        #--------------ACCEDER AL PROCESO------------------------
        
        driver.find_element_by_name("atencionCIC[detalle][motivo][id]").click()
        driver.find_element_by_css_selector("option[value='21']").click()

        driver.find_element_by_name("atencionCIC[detalle][observacion]").send_keys("Proceso CIC")

        time.sleep(5)
         
    #------GRABAR EL PROCESO------------

        driver.find_element_by_id("btnSave").click()
        time.sleep(3)
    
    #------CAPTURA DE PANTALLA------------

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_ETE\\tramiterechazarCIC.png")

        time.sleep(5)      

    #--------TOMAR TRAMITE

        driver = self.driver

        numerotramite = driver.find_element_by_css_selector("div[class='bootbox modal fade in']>div[class='modal-body']").text

        tramite = "#" + numerotramite[31:40]

        print("Tramite numero" + tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)        

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))
        time.sleep(3)

        #--------GESTION  Atencion CIC - Resolver

        observacionescic = "atencionCIC[detalle][observacionResolver]"

        driver.find_element_by_name(observacionescic).send_keys("Atencion CIC - Rechazada")

        time.sleep(3)
        
        #--------ACCION ----CONSULTA RECHAZADA

        rechazada = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/select/option[3]"

        driver.find_element_by_xpath(rechazada).click()
        time.sleep(3)

        flecha = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha).click()
        time.sleep(3)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(3)  
        
       #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))
        time.sleep(3)

        #--------GESTION  Atencion CIC - Resolver

        observacionesrechazada = "atencionCIC[detalle][observacionRechazar]"

        driver.find_element_by_name(observacionesrechazada).send_keys("Atencion CIC - Consulta Rechazada")

        time.sleep(3)  

        finalizar = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/select/option[2]"

        driver.find_element_by_xpath(finalizar).click()
        time.sleep(3)

        flecha = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha).click()
        time.sleep(3)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

         #--------INGRESO AL TRAMITE

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).move_to_element(icono).perform()
    
        #-------CAPTURA DE PANTALLA  

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_ETE\\rechazotramiteCIC.png")

        time.sleep(5)    

    def test03_completarinformacion(self):  

        driver = self.driver

        #--------------ACCEDER AL PROCESO------------------------
        
        driver.find_element_by_name("atencionCIC[detalle][motivo][id]").click()
        driver.find_element_by_css_selector("option[value='21']").click()

        driver.find_element_by_name("atencionCIC[detalle][observacion]").send_keys("Proceso CIC")

        time.sleep(5)
         
    #------GRABAR EL PROCESO------------

        driver.find_element_by_id("btnSave").click()
        time.sleep(3)
    
    #------CAPTURA DE PANTALLA------------

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_ETE\\tramitecompletarCIC.png")

        time.sleep(5)      

    #--------TOMAR TRAMITE

        driver = self.driver

        numerotramite = driver.find_element_by_css_selector("div[class='bootbox modal fade in']>div[class='modal-body']").text

        tramite = "#" + numerotramite[31:40]

        print("Tramite numero" + tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)        

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))
        time.sleep(3)

        #--------GESTION  Atencion CIC - C0MPLETAR INFORMACION

        observacionescic = "atencionCIC[detalle][observacionResolver]"

        driver.find_element_by_name(observacionescic).send_keys("Atencion CIC - Resolver")

        time.sleep(3)
        
        #--------ACCION ----COMPLETAR INFORMACION

        completar = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/select/option[4]"

        driver.find_element_by_xpath(completar).click()
        time.sleep(3)

        flecha = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha).click()
        time.sleep(3)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(3)  
        
       #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))
        time.sleep(3)

        #--------GESTION  Atencion CIC - Resolver

        observacioncompletar = "atencionCIC[detalle][observacionCompletar]"

        driver.find_element_by_name(observacioncompletar).send_keys("Atencion CIC - Completar Informacion")

        time.sleep(3)  

        finalizar = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/select/option[3]"

        driver.find_element_by_xpath(finalizar).click()
        time.sleep(3)

        flecha = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha).click()
        time.sleep(3)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(3)  

         #--------INGRESO AL TRAMITE

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).move_to_element(icono).perform()

        #-------CAPTURA DE PANTALLA  

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_ETE\\completarinformacionCIC.png")

        time.sleep(5)    


    def test04_comunicarsocio(self):  
 
        driver = self.driver

        #--------------ACCEDER AL PROCESO------------------------
        
        driver.find_element_by_name("atencionCIC[detalle][motivo][id]").click()
        driver.find_element_by_css_selector("option[value='21']").click()

        driver.find_element_by_name("atencionCIC[detalle][observacion]").send_keys("Proceso CIC")

        time.sleep(5)
         
    #------GRABAR EL PROCESO------------

        driver.find_element_by_id("btnSave").click()
        time.sleep(3)
    
    #------CAPTURA DE PANTALLA------------

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_ETE\\tramitecomunicarsocioCIC.png")

        time.sleep(5)      

    #--------TOMAR TRAMITE

        numerotramite = driver.find_element_by_css_selector("div[class='bootbox modal fade in']>div[class='modal-body']").text

        tramite = "#" + numerotramite[31:40]

        print("Tramite numero" + tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)        

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))
        time.sleep(3)

        #--------GESTION  Atencion CIC - C0MPLETAR INFORMACION

        observacionescic = "atencionCIC[detalle][observacionResolver]"

        driver.find_element_by_name(observacionescic).send_keys("Atencion CIC - Resolver")

        time.sleep(3)
        
        #--------ACCION ----COMUNICAR SOCIO

        completar = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/select/option[5]"

        driver.find_element_by_xpath(completar).click()
        time.sleep(3)

        flecha = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha).click()
        time.sleep(3)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(3)  
        
       #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))
        time.sleep(3)

        #--------GESTION  Atencion CIC - COMUNICAR SOCIO

        observacioncompletar = "atencionCIC[detalle][observacionComunicar]"

        driver.find_element_by_name(observacioncompletar).send_keys("Atencion CIC - Comunicar Resolucion")

        time.sleep(3)  

        comunicar = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/select/option[2]"

        driver.find_element_by_xpath(comunicar).click()
        time.sleep(3)

        flecha = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha).click()
        time.sleep(3)

        #--------BUSCAR TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(3)  

         #--------INGRESO AL TRAMITE

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).move_to_element(icono).perform()

        #-------CAPTURA DE PANTALLA  

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_ETE\\comunicarsocioCIC.png")

        time.sleep(5)           

if __name__ == '__main__':
    unittest.main()
