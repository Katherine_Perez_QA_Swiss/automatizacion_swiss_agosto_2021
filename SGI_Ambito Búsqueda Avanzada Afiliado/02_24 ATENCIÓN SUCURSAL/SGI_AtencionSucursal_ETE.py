import unittest
from unittest.main import main
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys


class AtencionSucursal(unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

    def test_Atencion(self):

        #ACCESO A SGI

        driver = self.driver
        driver.maximize_window()

        url = "http://sgipre/sgi-app/#login"
        url2= "http://sgiqa/sgi-app/#login"

        driver.get(url)
        time.sleep(3)

        #VERIFICAR EL TITULO DE SGI 

        titulopage = driver.title

        assertEqual = self.assertEqual

        assertEqual = ("SGI", titulopage, "No se logró acceder a SGI")

        #DATOS DE ACCESO SGI

        usuario = "ext_roperalt"
        contrasena = "Qactions01"
        user_id = "userName"
        password_id = "password"
        ingresar_button = "disable"

        driver.find_element_by_id(user_id).send_keys(usuario)
        driver.find_element_by_id(password_id).send_keys(contrasena)
        driver.find_element_by_id(ingresar_button).click()
        time.sleep(3)

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "29197733"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = "//option[@id='41']"
        ejecutivo_id = "CN=Roque Damian Peralta,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath).click()
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_ETE\\atencion_sucursal.png")
        time.sleep(3) 

if __name__ == '__main__':
    unittest.main()
    



