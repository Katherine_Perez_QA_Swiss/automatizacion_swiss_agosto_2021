import unittest
from unittest.main import main
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
#from webdriver_manager.chrome import ChromeDriverManager


class AtencionSucursal(unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")
        #self.driver = webdriver.Chrome(ChromeDriverManager().install())

    def test_Atencion(self):

        #ACCESO A SGI

        driver = self.driver
        driver.maximize_window()

        url = "http://sgipre/sgi-app/#login"
        url2= "http://sgiqa/sgi-app/#login"

        driver.get(url)
        time.sleep(7)

        #VERIFICAR EL TITULO DE SGI 

        titulopage = driver.title

        assertEqual = self.assertEqual

        assertEqual = ("SGI", titulopage, "No se logró acceder a SGI")

        #DATOS DE ACCESO SGI

        usuario = "EXT_MaFernan"
        contrasena = "u2y9fXxch"
        user_id = "userName"
        password_id = "password"
        ingresar_button = "disable"

        driver.find_element_by_id(user_id).send_keys(usuario)
        driver.find_element_by_id(password_id).send_keys(contrasena)
        driver.find_element_by_id(ingresar_button).click()
        time.sleep(3)        

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)    

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']","//option[@value='Censo Internaciones']"]
        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[0]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba Automatizacion")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #--------TOMAR TRAMITE

        numerotramite = driver.find_element_by_id("labelProcess").text

        tramite = numerotramite[27:38]

        print("Tramite numero" + tramite)
        
        time.sleep(2)        

        derivar_caja_xpath = "//a[@data-original-title='Derivar a Caja']//i"
        ok_xpath = "//body/div/div/a[2]"

        driver.find_element_by_xpath(derivar_caja_xpath).click()
        time.sleep(2)

        #EVIDENCIA CP ATENDER SOCIO --01

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\Socio-01.png")
        time.sleep(3) 

        driver.find_element_by_xpath(ok_xpath).click()
        time.sleep(3)  


        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)

        #EVIDENCIA CP ATENDER SOCIO --01-01

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\Socio-01-01.png")
        time.sleep(3) 

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(8)

        tramites_caja_id = "44-link-tab"

        driver.find_element_by_id(tramites_caja_id).click()
        time.sleep(4)

        #EVIDENCIA CP ATENDER SOCIO --01-02

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\Socio-01-02.png")
        time.sleep(3) 


        #-----CP 02 ATENCION SOCIO

        finalizar_atencion_xpath = "//a[@data-original-title='Finalizar Atención']//i"

        driver.find_element_by_xpath(finalizar_atencion_xpath).click()
        time.sleep(3)

        #EVIDENCIA CP ATENDER SOCIO --02

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\Socio-02.png")
        time.sleep(3) 

        driver.find_element_by_xpath(ok_xpath).click()
        time.sleep(3) 

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)

        #EVIDENCIA CP ATENDER SOCIO --02

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\Socio-02-01.png")
        time.sleep(3) 








if __name__ == '__main__':
    unittest.main()
    



