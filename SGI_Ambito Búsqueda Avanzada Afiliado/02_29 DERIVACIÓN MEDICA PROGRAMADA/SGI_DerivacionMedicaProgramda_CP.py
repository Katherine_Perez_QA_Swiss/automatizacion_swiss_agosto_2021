import unittest
from unittest.main import main
from selenium import webdriver
import re 
import time
import autoit
from selenium.webdriver.common.keys import Keys
import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoSuchFrameException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException


class DerivacionMedica (unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

        #ACCESO A SGI 

        driver = self.driver

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"

        driver.maximize_window()
        driver.get(url2)
        titulopage = driver.title
        driver.implicitly_wait(15)

        # verify title is SGI

        assertEqual = self.assertEqual
        assertEqual("SGI", titulopage, "No pudo acceder a SGI")     
        
        #DATOS DE USUARIO 

        usuario = "EXT_KaPerez"
        contrasenia = "mpDA4Hw6"
        username_id = "userName"
        password = "password"
        #disable_id = "disable"

        driver.find_element_by_id(username_id).send_keys(usuario)
        #driver.find_element_by_id(password_id).send_keys(contrasena)
        #driver.find_element_by_id(disable_id).click()

        password_id = WebDriverWait(driver, 10, 0.5).until(
        EC.presence_of_element_located((By.ID, password)))
        password_id.send_keys(contrasenia)
        password_id.submit()          

        #BUSCAR

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        buscar = WebDriverWait(driver, 10, 0.5).until(
        EC.presence_of_element_located((By.XPATH, buscar_xpath)))
        buscar.click()        

        #driver.find_element_by_xpath(buscar_xpath).click()
        #time.sleep(8)

        #INGRESO DE DU

        DU = "29197733"
        afiliado = "txtAfiliadoDocumento"
        boton = "btnSearch2"

        driver.find_element_by_id(afiliado).send_keys(DU)
        #driver.find_element_by_id(boton).click()

        boton = WebDriverWait(driver, 10, 0.5).until(
        EC.presence_of_element_located((By.ID, "btnSearch2")))
        boton.click()         

        #MOVER HASTA LA RUEDA

        rueda_ccsselector = "#ace-settings-btn"

        buscarueda = driver.find_element_by_css_selector(rueda_ccsselector)
        webdriver.ActionChains(driver).click_and_hold(buscarueda).perform()

        #SELECCIONAR UN PROCESO

        proceso_id = "inputProcess"
        nombre = "Deri"
        numero_id = "109"        
        
        driver.find_element_by_id(proceso_id).send_keys(nombre)
        time.sleep(2)
        proceso_id = driver.find_element_by_id(numero_id).click()
        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP1.png")
        time.sleep(5)       

        #ACCEDER AL IFRAME PROCESO

        iframe = "my_frame109" 

        #driver.switch_to.frame(driver.find_element_by_id(iframe))
        #time.sleep(5) 

        WebDriverWait(driver, 10, 0.5).until(
        EC.frame_to_be_available_and_switch_to_it((By.ID, iframe )))      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP2.png")
        time.sleep(5)          
    
    def test01_Derivacion(self):

        #------METODO PARA FECHA------------  

        base = datetime.date.today()

        fechahoy = base.strftime("%d/%m/%Y")
        
        driver = self.driver

        

        #GESTION DE PROCESO

        #SWICHT ¿Se verificó la existencia de otro prestador?
               
        prestador_id = "otroPrestador"

        driver.find_element_by_id(prestador_id).click()

        #SWICHT ¿Se controló el cuadro de convenio de OOSS?

        convenio_id = "convenioOOSS"

        #driver.find_element_by_id(convenio_id).click()
        
        convenio_oss = WebDriverWait(driver, 10, 0.5).until(
        EC.presence_of_element_located((By.ID, convenio_id)))  
        convenio_oss.click()   

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file(f"C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP3.png")
        
        #LISTA DESPLEGABLE : ¿Cuál es el motivo de ingreso?

        lista_motivo_ingreso_id = "motivosIngreso"

        #driver.find_element_by_id(lista_motivo_ingreso_id).click()
        #time.sleep(5)

        motivo = WebDriverWait(driver, 10, 0.5).until(
        EC.visibility_of_element_located((By.ID, lista_motivo_ingreso_id)))  
        motivo.click() 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP4.png")
        

        motivo_xpath = ["//option[contains(text(),'Amparo legal')]",
                        "//option[contains(text(),'Control postquirúrgico/Tratamiento')]",
                        "//option[contains(text(),'Extensión de derivación')]",
                        "//option[contains(text(),'Falta prestador en zona')]",
                        "//option[contains(text(),'Prestador existente no atiende patología')]"]         

       

        #LISTA DESPLEGABLE : ¿Cuál es el motivo de ingreso?-----OPCIÓN Control postquirúrgico/Tratamiento CP 06

        tipo_servicio = driver.find_element_by_id("tipoDeServicio")

        webdriver.ActionChains(driver).move_to_element(tipo_servicio).perform()
                 
        
        driver.find_element_by_xpath(motivo_xpath[1]).click()
        

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP6.png")
        

        #LISTA DESPLEGABLE : ¿Cuál es el motivo de ingreso?-----INGRESAR CARACTERES EN CANTIDAD DE CONTROLES CP 48y CP49

        #cantidad_controles_name = "solicitudDMP[cantidadDeControles]"

        #driver.find_element_name(cantidad_controles_name).send_keys("123")
        #time.sleep(3)

        #CAPTURE PANTALLA

        #driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP48yCP49.png")
        #time.sleep(5)

        #LISTA DESPLEGABLE : ¿Cuál es el motivo de ingreso?-----OPCIÓN Extensión de derivación CP 07

        #driver.find_element_by_xpath(motivo_xpath[2]).click()
        

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP7.png")
        

        #LISTA DESPLEGABLE : ¿Cuál es el motivo de ingreso?-----OPCIÓN Falta prestador en zona CP 08

        driver.find_element_by_xpath(motivo_xpath[3]).click()
        

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP8.png")
        

        #LISTA DESPLEGABLE : ¿Cuál es el motivo de ingreso?-----OPCIÓN Prestador existente no atiende patología CP 09

        driver.find_element_by_xpath(motivo_xpath[4]).click()
       

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP9.png")
        

        #LISTA DESPLEGABLE : ¿Cuál es el motivo de ingreso?-----OPCIÓN Amparo legal CP 05

        driver.find_element_by_xpath(motivo_xpath[0]).click()
        

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP5.png")
        
        
        #LISTA DESPLEGABLE : Tipo de Servicio----OPCION ATENCION DEMANDA-----CP 19

        servicio_xpath = ["//option[contains(text(),'( Todos )')]",
                        "//form[@novalidate='novalidate']//option[@value='8']" ]                         
        
        driver.find_element_by_xpath(servicio_xpath[0]).click()
        

        #LISTA DESPLEGABLE : Especialidad

        especialidad_id= "especialidad"                              

        especialidad = driver.find_element_by_id(especialidad_id).click()
        

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP19.png")
       
        #LUPA DIAGNOSTICO

        lupa_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//a[1]//i[1]"
        driver.find_element_by_xpath(lupa_xpath).click()        

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP30.png")
        
        #POP UP BUSQUEDA AVANZADA DIAGNOSTICOS---CODIGO

        codigo_id = "icdCodigoSearchsolicitudDMP"

        driver.find_element_by_id(codigo_id).send_keys("R05")

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP31.png")
       
        descripcion_id_diagnotisco = "icdDescripcionSearchsolicitudDMP"

        driver.find_element_by_id(descripcion_id_diagnotisco).send_keys("Tos")

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP32.png")
       

        #HACER CLIC EN EL BOTÓN CERRAR---- CP 33-----ROBOT PROBLEMAS DE RESOLUCION

        #PRESIONAR EL BOTÓN CERRAR

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)
        
        action.key_down(Keys.ENTER)

        action.perform()

        #button_cerrar_xpath = "//body/div[@id='page-content']/div[1]/div[1]/div[1]/div[10]/div[1]/div[3]/button[1]"

        #driver.find_element_by_xpath(button_cerrar_xpath).click()

        #time.sleep(5)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP33.png")

        #HACER CLIC EN EL BOTÓN CERRAR---- CP 34

        driver.find_element_by_xpath(lupa_xpath).click()

        button_cerrar_rojo_xpath = "//body/div/div/div/div/div/div/div/div/div/div/span/a/i[1]"

        driver.find_element_by_xpath(button_cerrar_rojo_xpath).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP34.png")

        #HACER CLIC EN EL BOTÓN BUSCAR DENTRO DEL POPUP ---- CP 35-----PROBLEMAS DE RESOLUCION

        driver.find_element_by_xpath(lupa_xpath).click()

        driver.find_element_by_id(descripcion_id_diagnotisco).send_keys("Tos")      

        #POP UP BUSQUEDA AVANZADA DIAGNOSTICOS---BUSCAR

        buscar_button_id =  "solicitudDMPbtnSearch"

        driver.find_element_by_id(buscar_button_id).click()

        #PRESIONAR EL BOTÓN BUSCAR

        #action = webdriver.ActionChains(driver)

        #action.key_down(Keys.TAB)

        #time.sleep(1) 

        #action.key_up(Keys.TAB)

        #time.sleep(1)         
        
        #action.key_down(Keys.ENTER)

        #action.perform()

        #time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP35.png")

        driver.find_element_by_id(descripcion_id_diagnotisco).clear()     

        driver.find_element_by_id(codigo_id).send_keys("R05") 

        time.sleep(3)

        #buscar = WebDriverWait(driver, 10, 0.5).until(
        #EC.visibility_of_element_located((By.ID, buscar_button_id)))
        #buscar.click() 

        driver.find_element_by_id(buscar_button_id).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP35_1.png")    

        #SELECCIONAR DIAGNOSTICO PARA VERIFICAR CAMPO ICD ---- CP 36         

        solicitud_icd_id = driver.find_element_by_id("solicitudDMP-icd1")

        webdriver.ActionChains(driver).move_to_element(solicitud_icd_id).perform()
      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP36.png")
     

        #SELECCIONAR ICONO DE LIMPIAR ---- CP 37

        button_limpiar_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//a[2]//i[1]"

        driver.find_element_by_xpath(button_limpiar_xpath).click()   

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP37.png")
   
        #Presenta documentación-----Resumen de historia clínica

        resumen_id = "documento-0"

        driver.find_element_by_id(resumen_id).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP38.png")
  
        #Presenta documentación-----Orden médica

        orden_id = "documento-1"

        driver.find_element_by_id(orden_id).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP39.png")
 
        #Presenta documentación-----Estudios Previos

        estudios_previos_id = "documento-2"

        driver.find_element_by_id(estudios_previos_id).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP40.png")
      

        #ADJUNTAR DOCUMENTO

        adjuntar_xpath = "//i[@data-rel='tooltip']"

        driver.find_element_by_xpath(adjuntar_xpath).click()

        time.sleep(5)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP41.png")
     
        #CARGAR ARCHIVOS ---- CP 42

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click() 

        time.sleep(8)
        #adjuntar_archivo = WebDriverWait(driver, 10).until(
        #EC.element_to_be_clickable((By.ID, cargararchivo)))
        #adjuntar_archivo.click()                    

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\maty")        

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")
        
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP42.png")
 
        #CERRAR POP UP 

        cerrar_button_xpath = "//span[contains(text(),'×')]"

        driver.find_element_by_xpath(cerrar_button_xpath).click()
 

        #CARGAR ARCHIVOS ---- CP 43  

        driver.find_element_by_xpath(adjuntar_xpath).click()  

        time.sleep(2)

        driver.find_element_by_id(cargararchivo).click() 

        time.sleep(2)          
   
        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\Defectos por PX")        

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")
 
        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.perform()
        
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP43.png")
        

        #BORRAR ARCHIVOS ---- CP 44 

        driver.find_element_by_xpath(adjuntar_xpath).click()

        adjuntar_archivo = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.ID, cargararchivo)))
        adjuntar_archivo.click()        

        #driver.find_element_by_id(cargararchivo).click()           

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\maty")        

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")  

        seleccionar_archivo_xpath = "//body/div[@id='page-content']/div[1]/div[1]/div[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[2]/ul[1]/li[1]/div[1]/div[1]"  

        seleccionar_archivo = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, seleccionar_archivo_xpath)))
        seleccionar_archivo.click()        
        
        #driver.find_element_by_xpath(seleccionar_archivo_xpath).click()   

        borrar_archivo_xpath = "//a[@data-original-title='Borrar seleccionadas']//i"

        #borrar_archivo = WebDriverWait(driver, 10).until(
        #EC.element_to_be_clickable((By.XPATH, borrar_archivo_xpath)))
        #borrar_archivo.click()

        time.sleep(5)

        driver.find_element_by_xpath(borrar_archivo_xpath).click()   

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP44.png")

        #PRESIONAR EL BOTÓN CANCELAR

        time.sleep(5)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB) 

        action.key_down(Keys.TAB)  

        action.key_up(Keys.TAB)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.TAB)   

        action.key_up(Keys.TAB)   

        action.key_down(Keys.ENTER)

        action.perform()

        #CAPTURE PANTALLA----aqui

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP45.png")

        #CERRAR  ---- CP 46

        adjuntar_xpath = "//i[@data-rel='tooltip']"         

        driver.find_element_by_xpath(adjuntar_xpath).click()        

        time.sleep(5)

        driver.find_element_by_xpath(cerrar_button_xpath).click()      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP46.png")
        time.sleep(5) 

        #Validación el boton "Relacionar llamado"-----CP54

        relacionar_tramite_xpath = "//button[@data-rel='tooltip']//i"

        #relacionar_tramite = WebDriverWait(driver, 10).until(
        #EC.element_to_be_clickable((By.XPATH, relacionar_tramite_xpath)))
        #relacionar_tramite.click()

        driver.find_element_by_xpath(relacionar_tramite_xpath).click() 

        time.sleep(5)   

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP54.png")
        
        #VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE-----10 TRAMITES

        lista = "listMyTaskGrid_length"

        driver.find_element_by_name(lista).click() 

        time.sleep(10)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP55.png")
        
        #VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 25 TRAMITES

        lista25 = "//div[@role='grid']//div//div//div//option[@value='25']"

        driver.find_element_by_xpath(lista25).click() 

        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP56.png")

        #VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 50 TRAMITES

        lista50 = "//option[@value='50']"

        driver.find_element_by_xpath(lista50).click() 

        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP57.png")

        #VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 100 TRAMITES

        lista100 = "//option[@value='100']"

        driver.find_element_by_xpath(lista100).click() 

        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP58.png")

        #VALIDAR AVANCE DE PAG 
        
        flecha = "2"

        driver.find_element_by_link_text(flecha).click()

        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP59.png")

        #VALIDAR CIERRE DE POP UP RELACIONAR TRAMITES

        driver.find_element_by_xpath("//body/div/div/div/div/div/div/div/div/div/span/a[@href='#']/i[1]").click()

        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP70.png")

        #cerrar_relacionar_tramite_xpath = "//body/div/div/div/div/div/div/div/div/div/span/a[@href='#']/i[1]"        

        #cerrar_relacionar_tramite = WebDriverWait(driver, 10).until(
        #EC.element_to_be_clickable((By.XPATH, cerrar_relacionar_tramite_xpath)))
        #cerrar_relacionar_tramite.click()

        #driver.find_element_by_xpath(cerrar_relacionar_tramite_xpath).click()        

        #Validación de campo obligatorio "Tipo de servicio"-----CP73 y CP74

        driver.find_element_by_xpath(motivo_xpath[0]).click()
        time.sleep(5)

        #SIGUIENTE A PASO 2

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP73yCP74.png")
        time.sleep(5)

        tipo_servicio_xpath = "//option[contains(text(),'Atención Demanda Espontánea')]"

        driver.find_element_by_xpath(tipo_servicio_xpath).click()
        time.sleep(2)   

        #Validación de campos obligatorios "ICD" y "Descripción" se la sección "Diagnostico"-----CP75 
                  
        #LISTA DESPLEGABLE : Especialidad

        especialidad_id= "especialidad"                              

        especialidad = driver.find_element_by_id(especialidad_id)
        
        webdriver.ActionChains(driver).move_to_element(especialidad).perform()        

        especialidad_xpath2 = "//option[@id='6']"        
                              
        driver.find_element_by_xpath(especialidad_xpath2).click()       

        #MOVER HASTA EL CAMPO ICD PARA CAPTURA CORRECTA

        solicitud_icd_id = driver.find_element_by_id("solicitudDMP-icd1")

        webdriver.ActionChains(driver).move_to_element(solicitud_icd_id).perform()        

        #SIGUIENTE A PASO 2       

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP75.png")
         
        #LUPA DIAGNOSTICO

        driver.find_element_by_xpath(lupa_xpath).click()

        time.sleep(3)        
        
        #POP UP BUSQUEDA AVANZADA DIAGNOSTICOS---CODIGO

        driver.find_element_by_id(codigo_id).send_keys("R05")

        time.sleep(3)

        driver.find_element_by_id(buscar_button_id).click()  

        time.sleep(3)   

        driver.find_element_by_id(resumen_id).click()            

        driver.find_element_by_id(orden_id).click() 
       
        driver.find_element_by_id(estudios_previos_id).click()
        
        #SIGUIENTE A PASO 2       

        driver.find_element_by_id(siguiente_id).click()        

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP76.png")
       
        #Validación de la sección "Adjuntar archivos" como obligatoria

        driver.find_element_by_id(resumen_id).click()       

        #SIGUIENTE A PASO 2       

        driver.find_element_by_id(siguiente_id).click()
       
        time.sleep(8)
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP77.png")
        
        #ADJUNTAR DOCUMENTO

        adjuntar_xpath = "//i[@data-rel='tooltip']"

        driver.find_element_by_xpath(adjuntar_xpath).click()
        time.sleep(2)

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\maty")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(10)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)

        #SIGUIENTE A PASO 2

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()  

        time.sleep(5)      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP78.png")
       
        #PASO 2 Validación del boton "Buscar" de la sección "Donde/Quien realizara el procedimiento"---CP79

        lupa_buscar_profesional_xpath = "//a[@id='botonBuscarProfesionalEfector']"

        #lupa_buscar = WebDriverWait(driver, 10).until(
        #EC.visibility_of_element_located((By.XPATH, lupa_buscar_profesional_xpath)))
        #lupa_buscar.click()

        driver.find_element_by_xpath(lupa_buscar_profesional_xpath).click()   
        
        time.sleep(5)      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP79.png")
        
        #PASO 2 Validación de ingreso de caracteres en el campo "Codigo---CP80

        codigo_prestador_xpath = "//input[@id='codigoBusqueda']"

        driver.find_element_by_xpath(codigo_prestador_xpath).send_keys("AAAA")     

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP80.png")
      

        #PASO 2 Validación de ingreso de caracteres en el campo "Cuit"---CP81

        cuit_prestador_xpath = "//input[@id='cuitBusqueda']"

        driver.find_element_by_xpath(cuit_prestador_xpath).send_keys("AAAA")       

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP81.png")
       
        #PASO 2 Validación de ingreso de caracteres en el campo "Apellido/Razon social---CP82

        apellido_prestador_xpath = "//input[@id='apellidoRazonSocialBusqueda']"

        driver.find_element_by_xpath(apellido_prestador_xpath).send_keys("AAAA1234")                    

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP82.png")
       
        driver.find_element_by_xpath(apellido_prestador_xpath).clear()     

        #PASO 2 Validación de ingreso de caracteres en el campo "Nombre"---CP83

        nombre_prestador_xpath = "//input[@id='nombreBusqueda']"

        driver.find_element_by_xpath(nombre_prestador_xpath).send_keys("AAAA1234?¡?¡?")                     

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP83.png")
      
        driver.find_element_by_xpath(nombre_prestador_xpath).clear()  

        time.sleep(5)     

        #PASO 2 Validación de resultado de busqueda sin ningun dato ingresa---CP84

        buscar_prestador_xpath = "//button[@id='btnBusquedaPrestadorBuscar']"

        driver.find_element_by_xpath(buscar_prestador_xpath).click()                      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP84.png")
        
        #PASO 2 Validación de resultado de busqueda con datos ingresado---CP85 Y CP86

        driver.find_element_by_xpath(codigo_prestador_xpath).send_keys("1130")
        time.sleep(1) 

        driver.find_element_by_xpath(buscar_prestador_xpath).click()
        time.sleep(1) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP85yCP86.png")
       
        #PASO 2 Validación del boton "Cancelar"---CP87

        cancelar_prestador_id = "cancelModalListadoPrestadores"

        driver.find_element_by_id(cancelar_prestador_id).click()

        time.sleep(5)       

        #CAPTURE PANTALLA
        
        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP87.png")
        
        #PASO 2 Validación del boton "Cerrar"---CP88

        driver.find_element_by_xpath(lupa_buscar_profesional_xpath).click()     
        
        time.sleep(5) 

        cerrar_prestador_xpath = "//div[@role='dialog']//form[@method='get']//div[1]//button[1]"

        driver.find_element_by_xpath(cerrar_prestador_xpath).click()  
          
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP88.png")
        
        #PASO 2 Validación de seleccionar un numero de codigo dentro del popup "Buscar prestador"---CP89

        driver.find_element_by_xpath(lupa_buscar_profesional_xpath).click()    

        time.sleep(1) 
        
        driver.find_element_by_xpath(codigo_prestador_xpath).send_keys("1130")     

        time.sleep(1)   

        driver.find_element_by_xpath(buscar_prestador_xpath).click()     

        time.sleep(1)   

        numero_buscar_prestador_xpath =  ("//a[contains(text(),'1130')]")

        time.sleep(1) 

        driver.find_element_by_xpath(numero_buscar_prestador_xpath).click()   

        time.sleep(3) 

        calle_buscar_prestador_xpath =  ("//a[contains(text(),'TTE GRAL JUAN D PERON')]")

        driver.find_element_by_xpath(calle_buscar_prestador_xpath).click()

        time.sleep(3)     

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP89.png")
     
        #PASO 2 Validación del boton "Limpiar"---CP90

        button_limpiar_id = "botonBorrarProfesionalEfector"

        driver.find_element_by_id(button_limpiar_id).click()       

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP90.png")
       
        #Validación del switch "Atención particular"----CP 91 Y CP92

        switch_atencion_id = "atencionParticularProfesionalEfector"

        driver.find_element_by_id(switch_atencion_id).click()        

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP91yCP92.png")
        
        #Validación ingreso de caracteres en el campo "Razon social"---CP 93

        razon_social_id = "apellidoRazonSocialProfesionalEfector"

        driver.find_element_by_id(razon_social_id).send_keys("AAA123")      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP93.png")
      
        #Validación ingreso de caracteres en el campo "Matricula"---CP 94

        matricula_id = "matriculaProfesionalEfector"

        driver.find_element_by_id(matricula_id).send_keys("AAA123")      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP94.png")
       
        #Validación ingreso de caracteres en el campo "E-Mail"---CP 95

        email_id = "emailProfesionalEfectorParticular"

        driver.find_element_by_id(email_id).send_keys("prueba@gmail.com")
       
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP95.png")
        
        #Validación de longitud de caracteres en el campo "Razon social"---CP96

        driver.find_element_by_id(razon_social_id).clear()
      
        driver.find_element_by_id(razon_social_id).send_keys("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefgh12345678901234567890123456789012")

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP96.png")
       
        driver.find_element_by_id(razon_social_id).clear()   

        #Validación de longitud de caracteres en el campo "Razon social"----CP 97

        telefono_id = "cargarNuevoTelefonoEfector"

        driver.find_element_by_id(telefono_id).click()      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP97.png")
     
        #VALIDACIÓN RADIO BUTTON TELEFONOS ----CP98

        telefono_celular_id = "prestadorPhoneteleTipoC"

        driver.find_element_by_id(telefono_celular_id).click()
      
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP98-1.png")
    
        telefono_fijo_id = "prestadorPhoneteleTipoF"

        driver.find_element_by_id(telefono_fijo_id).click()      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP98-2.png")
     
        #Validación del campo tipo combo "Provincia"----CP99

        provincia_xpath = "//form[@method='get']//div//div//div//div//div//div//div//div//option[@value='1']"

        driver.find_element_by_xpath(provincia_xpath).click()
   
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP99.png")
   
        #Validación del campo tipo combo "Localidad"----CP100

        localidad_xpath = "//form[@method='get']//div//div//div//div//div//div[3]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(localidad_xpath).click()       

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP100.png")

        #Validación del campo tipo combo "Partido"----CP 102

        partido_xpath = "//body/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div[2]/div[1]/select[1]/option[2]"

        driver.find_element_by_xpath(partido_xpath).click()
       
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP102.png")
   
        #Validación de ingreso de caracteres en el campo "Numero"----CP 104

        numero_id = "prestadorPhonenumero"

        driver.find_element_by_id(numero_id).send_keys("1123938926")
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP104.png")
  
        #Validación del boton "Limpiar"------CP 105

        button_clean_id = "prestadorPhonecleanPhoneButton"

        driver.find_element_by_id(button_clean_id).click()
       
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP105.png")

        #Validación del boton "Cancelar"-----CP 106-----PROBLEMAS DE RESOLUCION

        numero_id = "prestadorPhonenumero"

        driver.find_element_by_id(numero_id).click()           

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)   

        action.key_up(Keys.TAB)       

        action.key_down(Keys.ENTER)

        action.perform()     

        #button_cancelar_id = "cancelModalValidation"

        #driver.find_element_by_id(button_cancelar_id).click()
        #time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP106.png")

        #Validación del boton "Cerrar"-----CP 107

        driver.find_element_by_id(telefono_id).click()       

        cerrar_popup_id = "closeModalValidate"

        driver.find_element_by_id(cerrar_popup_id).click()     

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP107.png")

        #Validación de combinación de campos obligatorios----CP 108

        driver.find_element_by_id(telefono_id).click() 

        time.sleep(2)        

        #driver.find_element_by_id(numero_id).send_keys("1123938926")       

        #button_validar_id = "prestadorPhonevalidatePhoneButton"

        #driver.find_element_by_id(button_validar_id).click()  

        #time.sleep(5)  

        #WebDriverWait(driver, 30).until(
        #EC.visibility_of_element_located((By.XPATH, "//div[@id='gritter-notice-wrapper']")))   

        #CAPTURE PANTALLA

        #driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP108.png")
   
        #Validación del boton "Validar"---CP109

        #POP UP Nuevo Teléfono---PROVINCIA

        provincia_xpath = "//body/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div/div/select/option[2]"

        driver.find_element_by_xpath(provincia_xpath).click() 
        time.sleep(2)  

        #POP UP Nuevo Teléfono---PARTIDO

        partido_xpath = "//body/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div[2]/div[1]/select[1]/option[2]"

        driver.find_element_by_xpath(partido_xpath).click()  

        time.sleep(2)    

        #POP UP Nuevo Teléfono---LOCALIDAD

        localidad_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div[3]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(localidad_xpath).click()
        time.sleep(2)
      
        #POP UP Nuevo Teléfono---NUMERO

        numero_id2= "prestadorPhonenumero"              

        driver.find_element_by_id(numero_id2).send_keys("11239389")  

        time.sleep(3)           

        #POP UP Nuevo Teléfono---VALIDAR

        #validar_id = "prestadorPhonevalidatePhoneButton"

        #driver.find_element_by_id(validar_id).click()
        #time.sleep(5)

        #CAPTURE PANTALLA

        #driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP109.png")

        #Validación del boton "Validar"-----CP 110        

        driver.find_element_by_id(numero_id2).send_keys("26")
        time.sleep(2)
        
        #POP UP Nuevo Teléfono---VALIDAR

        validar_id = "prestadorPhonevalidatePhoneButton"

        driver.find_element_by_id(validar_id).click()    

        time.sleep(2)      

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP110.png")

        #Validación del boton "Confirmar"----CP 111

        #POP UP Nuevo Teléfono---CONFIRMAR                   
        
        action = webdriver.ActionChains(driver)
        
        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)           
        
        #confirmar_id = "phoneConfirmButton"

        #driver.find_element_by_id(confirmar_id).click()
        #time.sleep(4)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP111.png")

        #Validación del switch "Excepción"-----CP 112

        excepcionar_id = "excepcionar"

        driver.find_element_by_id(excepcionar_id).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP112.png")
 
        #FINALIZAR TRAMITE

        finalizar_id = "btnContinuar"

        driver.find_element_by_id(finalizar_id).click()
        time.sleep(3)

         #PASO 3

        #Nombre del Contacto

        nombre_name = "solicitudDMP[contacto][nombre]"

        driver.find_element_by_name(nombre_name).send_keys("Prueba")
        time.sleep(3)

        #Teléfono del Contacto

        telefono_button_id = "cargarNuevoTelefonoContacto"

        driver.find_element_by_id(telefono_button_id).click()
        time.sleep(5)

        #POP UP Nuevo Teléfono---PROVINCIA

        provincia_xpath = "//body/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div/div/select/option[2]"

        driver.find_element_by_xpath(provincia_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---PARTIDO

        partido_xpath = "//body/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div[2]/div[1]/select[1]/option[2]"

        driver.find_element_by_xpath(partido_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---LOCALIDAD

        localidad_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div[3]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(localidad_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---NUMERO

        numero_id2= "prestadorPhonenumero"

        driver.find_element_by_id(numero_id2).send_keys("1123938926")
        time.sleep(2)

        #POP UP Nuevo Teléfono---VALIDAR

        validar_id = "prestadorPhonevalidatePhoneButton"

        driver.find_element_by_id(validar_id).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---CONFIRMAR        
        
        action = webdriver.ActionChains(driver)
        
        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)        

        #FINALIZAR TRAMITE

        finalizar_id = "btnContinuar"

        driver.find_element_by_id(finalizar_id).click()
        time.sleep(3)       

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP1\\ETE\\GeneracionTramite.png")
   
        #TOMAR TRÁMITE
    
        numerotramite = driver.find_element_by_xpath("//*[@class='bootbox modal fade in']/child::div[1]").text

        #//body/div[7]/#body:nth-child(2) > div.bootbox.modal.fade.in:nth-child(8)/#/html/body/div[7]/div[1]

        print(numerotramite)

        busqueda_tramite = r"(?<=Trámite )\w+"

        numero_tramite = re.findall(str(busqueda_tramite),numerotramite, re.IGNORECASE)

        print(numero_tramite) 

        tramite = "#" + str(numero_tramite[0])
        
        print(tramite) 

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)           

        #--------BUSQUEDA INTELIGENTE TRAMITE            

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)

        driver.find_element_by_xpath("//input[@aria-controls='listUnassignTaskGrid']").send_keys(tramite)

        time.sleep(10)        

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\01-Inicio\\CP113.png")
        time.sleep(5)                 

        #ABRIR TRAMITE

        tomar_abrir_tramite_xpath = "//a[@data-original-title='Tomar y Abrir']//i"

        abrir_tramite_xpath = "//a[@data-original-title='Abrir']//i"

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_01.png")
        time.sleep(5) 

        #-------- Derivacion Medica Programada - Contactar Empresa-----SWICTH CUBRE PRESTACION
       
        cubre_prestacion_id = "chkCubrePrestacion"

        driver.find_element_by_id(cubre_prestacion_id).click()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_02.png")
        time.sleep(5) 

        #-------- Derivacion Medica Programada - Contactar Empresa-----SWICTH CUBRE TRASLADO/ALOJAMIENTO

        cubre_traslado_alojamiento_id = "chkCubreTrasladoAloj"

        driver.find_element_by_id(cubre_traslado_alojamiento_id).click()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_03.png")
        time.sleep(5)

        #-------- Derivacion Medica Programada - Contactar Empresa-----CAMPO OBSERVACION

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("prueba12345´+{--")
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_04.png")
        time.sleep(5)
        
        #-------- Derivacion Medica Programada - Contactar Empresa-----OPCIONES DISPONIBLES---Cont emp - 022

        accion_id = "actions"

        driver.find_element_by_id(accion_id).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_05.png")
        time.sleep(5)

        #-------Validación del boton "Ver y adjuntar archivos"-----Cont emp - 009

        button_adjuntar_id = "btnRUDI"

        driver.find_element_by_id(button_adjuntar_id).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_09.png")
        time.sleep(5) 

        #Validación del boton "Ver" de la imagen previamente adjuntada dentro del popup "Adjuntos del tramite" ----Cont emp - 011

        container_imagen = driver.find_element_by_class_name("archivoLiSubido")        

        webdriver.ActionChains(driver).click_and_hold(container_imagen).perform()
        time.sleep(3) 
        
        button_ver_archivo_xpath = "//i[@title='Ver archivo']"

        driver.find_element_by_xpath(button_ver_archivo_xpath).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_11.png")
        time.sleep(5)

        #--------Validación del boton "Descargar"-----Cont emp - 012

        container_imagen = driver.find_element_by_class_name("archivoLiSubido")        

        webdriver.ActionChains(driver).click_and_hold(container_imagen).perform()
        time.sleep(3) 

        button_ver_descarga_xpath = "//i[@title='Descargar']"

        driver.find_element_by_xpath(button_ver_descarga_xpath).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_12.png")
        time.sleep(5)

        #ADJUNTAR DOCUMENTO SEGUNDO DOCUMENTO

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_10.png")
        time.sleep(2)       

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\prueba")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(5) 

        #Validación del boton "Cargar" dentro del popup-------Cont emp - 018

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)

        #button_cargar_descarga_id = "btnArchivosAceptarRUDI"

        #driver.find_element_by_id(button_cargar_descarga_id).click()
        #time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_18.png")
        time.sleep(5)

        #Validación del boton "Cancelar" dentro del popup-----Cont emp - 019

        button_cancelar_descarga_xpath = "//a[contains(text(),'Cancelar')]"

        driver.find_element_by_xpath(button_cancelar_descarga_xpath).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_19.png")
        time.sleep(5)

        #---------Validación del boton "Borrar"-----Cont emp - 013

        button_borrar_descarga_xpath = "//i[@title='Borrar']"

        driver.find_element_by_xpath(button_borrar_descarga_xpath).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_13.png")
        time.sleep(5)  
        
        #Validación del boton "OK" dentro del popup-------Cont emp - 020  
        
        button_cerrar_xpath = "//span[contains(text(),'×')]"

        driver.find_element_by_xpath(button_cerrar_xpath).click()

        driver.find_element_by_id(button_adjuntar_id).click()
        time.sleep(2) 
        
        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\prueba2")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(5)       

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)        
        
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_20-1.png")
        
        time.sleep(5)

        presionar_ok_xpath = "//a[contains(text(),'OK')]"

        driver.find_element_by_xpath(presionar_ok_xpath).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\02-Contactar_Empresa\\Contactar_07.png")
        
        accion_swiss_xpath = "//option[@id='2']"

        driver.find_element_by_xpath(accion_swiss_xpath).click()
        time.sleep(2)

        flecha_id = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha_id).click()
        time.sleep(8)         
        
        
        #--------BUSQUEDA INTELIGENTE TRAMITE         

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(2)   

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)                     

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")
        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")
            
        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\03-Aprobar_Excepción\\Aprobar_Excepcion_1.png")
        time.sleep(5)                 

        #ABRIR TRAMITE

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)        

        #--------- Derivacion Medica Programada - Excepcionar carga Switch

        excepcionar_carga_id = "chkExcepcionaCarga"

        driver.find_element_by_id(excepcionar_carga_id).click()

        time.sleep(3)

        #--------- Derivacion Medica Programada - Motivo Excepcion

        motivo_excepcion_xpath = "//option[@value='1']"

        driver.find_element_by_xpath(motivo_excepcion_xpath).click()

        time.sleep(3)

        #--------- Derivacion Medica Programada - Aprobar Excepcion

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Aprobar Excepcion")

        time.sleep(3)

        #--------- Derivacion Medica Programada - CONTINUAR

        continuar_id = "1"

        driver.find_element_by_id(continuar_id).click()

        time.sleep(2)

        #--------- Derivacion Medica Programada - FLECHA

        flecha_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath).click()

        time.sleep(5)        

        #--------BUSQUEDA INTELIGENTE TRAMITE        

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(2)    

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_1.png")
        time.sleep(5)                 

        #ABRIR TRAMITE

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)        

        #--------- Derivacion Medica Programada - Asignar Auditor------Asig audi - 004

        lista_auditor_id = "auditores"
        
        auditor_xpath = "//option[@value='CN=Katherine Perez,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar']"

        observacion_id = "observacion"

        driver.find_element_by_id(lista_auditor_id).click()
        time.sleep(1)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_4.png")
        
        #--------- Derivacion Medica Programada - Asignar Auditor------Asig audi - 006       

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Asignar Auditor 1234567")
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_6.png")
        
       #--------- Derivacion Medica Programada - Asignar Auditor------Asig audi - 007
       
        driver.find_element_by_id(observacion_id).clear()

        driver.find_element_by_id(observacion_id).send_keys("123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345")

        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_7.png")
       
        driver.find_element_by_id(observacion_id).clear()

        #--------- Derivacion Medica Programada - Asignar Auditor------Asig audi - 009

        accion_id = "actions"

        driver.find_element_by_id(accion_id).click()
        time.sleep(1)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_9.png")
              
        
        #--------- Derivacion Medica Programada - Asignar Auditor------Asig audi - 010
        
        finalizar_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(finalizar_xpath).click()
        time.sleep(5)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_10.png")
              
        #--------- Derivacion Medica Programada - Validación del boton "Ver y adjuntar archivos"------Asig audi - 011     
        
        button_adjuntar_id = "btnRUDI"

        driver.find_element_by_id(button_adjuntar_id).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_11.png")
        time.sleep(5) 

        #Validación del boton "Ver" de la imagen previamente adjuntada dentro del popup "Adjuntos del tramite" ----Asig audi - 013

        container_imagen = driver.find_element_by_class_name("archivoLiSubido")        

        webdriver.ActionChains(driver).click_and_hold(container_imagen).perform()
        time.sleep(3) 
        
        button_ver_archivo_xpath = "//i[@title='Ver archivo']"

        driver.find_element_by_xpath(button_ver_archivo_xpath).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_13.png")
        time.sleep(5)

        #--------Validación del boton "Descargar"-----Asig audi - 014

        container_imagen = driver.find_element_by_class_name("archivoLiSubido")        

        webdriver.ActionChains(driver).click_and_hold(container_imagen).perform()
        time.sleep(3) 

        button_ver_descarga_xpath = "//i[@title='Descargar']"

        driver.find_element_by_xpath(button_ver_descarga_xpath).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_14.png")
        time.sleep(5)

        #ADJUNTAR DOCUMENTO SEGUNDO DOCUMENTO

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        #CAPTURE PANTALLA----Validación del boton "Seleccionar archivo" dentro del popup "Adjuntos del tramite"--Asig audi - 012

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_12.png")
        time.sleep(2)       

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\prueba")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(5) 

        #Validación del boton "Cargar" dentro del popup-------Asig audi - 020

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)

        #button_cargar_descarga_id = "btnArchivosAceptarRUDI"

        #driver.find_element_by_id(button_cargar_descarga_id).click()
        #time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_20.png")
        time.sleep(5)

        #Validación del boton "Cancelar" dentro del popup-----Asig audi - 018

        button_cancelar_descarga_xpath = "//a[contains(text(),'Cancelar')]"

        driver.find_element_by_xpath(button_cancelar_descarga_xpath).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_18.png")
        time.sleep(5)

        #---------Validación del boton "Borrar"-----Asig audi - 015

        button_borrar_descarga_xpath = "//i[@title='Borrar']"

        driver.find_element_by_xpath(button_borrar_descarga_xpath).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_15.png")
        time.sleep(5)  
        
        #Validación del boton "Cerrar" dentro del popup "Adjuntos del tramite"-----Asig audi - 019
        
        button_cerrar_xpath = "//span[contains(text(),'×')]"

        driver.find_element_by_xpath(button_cerrar_xpath).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_19.png")
        time.sleep(5)

        #Validación del boton "OK" dentro del popup-------Asig audi - 022

        driver.find_element_by_id(button_adjuntar_id).click()
        time.sleep(2) 
        
        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\descarga")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(5)       

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)   

        presionar_ok_xpath = "//a[contains(text(),'OK')]"

        driver.find_element_by_xpath(presionar_ok_xpath).click()
        time.sleep(2)
               
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\04-Asignar_Auditor\\Asignar_Auditor_22.png")
               
        #--------- Derivacion Medica Programada - Asignar Auditor---ACCION

        driver.find_element_by_xpath(auditor_xpath).click()
        time.sleep(2)

        accion_id = "1"        

        driver.find_element_by_id(accion_id).click()
        time.sleep(2)

        flecha_id = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha_id).click()
        time.sleep(8)      
      
        
        #--------BUSQUEDA INTELIGENTE TRAMITE              

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(6)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)     

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                       

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")
        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")               

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE
        
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()

        time.sleep(5) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\05-Auditar_Medicamente\\AuditarMedicamente_1.png")      
        
        time.sleep(5)

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8) 
        
        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)
        
        #-------- Derivacion Medica Programada - Validación del switch "Continua con la auditoria"----Audi med - 007

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\05-Auditar_Medicamente\\AuditarMedicamente_7.png")      
               
        #-------- Derivacion Medica Programada - Validación del switch "Continua con la auditoria" con opción "NO"----Audi med - 008
                
        continua_auditoria_switch_id = "chkContinuaAuditoria"

        driver.find_element_by_id(continua_auditoria_switch_id).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\05-Auditar_Medicamente\\AuditarMedicamente_8.png")      
        
        #-------- Derivacion Medica Programada -Validación de radio button "Contactar al socio porque no se aprueba la derivación" con switch "Continua con la auditoria" con opción "NO"

        radio_button_contactar_al_socio_xpath = "//input[@value='rechazaMedico']"

        driver.find_element_by_xpath(radio_button_contactar_al_socio_xpath).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\05-Auditar_Medicamente\\AuditarMedicamente_9.png")   

        #-------- Derivacion Medica Programada --Validación de radio button "AUDITOR BS.AS" con switch "Continua con la auditoria" con opción "NO"

        radio_button_auditor_xpath = "//input[@value='BSAS']"

        driver.find_element_by_xpath(radio_button_auditor_xpath).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\05-Auditar_Medicamente\\\AuditarMedicamente_10.png")  

        driver.find_element_by_id(continua_auditoria_switch_id).click()        
        
        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 2----REQUIERE AUTORIZACION

        requiere_autorizacion_swith_id = "requiereAutorizacion"

        driver.find_element_by_id(requiere_autorizacion_swith_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 2----REQUIERE MATERIALES Y PROTESIS

        requiere_materiales_protesis_swith_id = "requiereMP"

        driver.find_element_by_id(requiere_materiales_protesis_swith_id).click()
        time.sleep(4)

        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 2

        #Derivación Regional (de 50 a 150 km)

        derivacion_name = "solicitudDMP[tipoDerivacion]"

        driver.find_element_by_name(derivacion_name).click()
        time.sleep(2)   

        #Tipo Traslado---AEREO

        aereo_xpath = "//input[@value='A']"
        driver.find_element_by_xpath(aereo_xpath).click()
        time.sleep(2)        

        #Cantidad de Días

        cantidad_id = "cantidadDias"

        driver.find_element_by_id(cantidad_id).send_keys("1")
        time.sleep(5)        

        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 2----APRUEBA ACOMPAÑANTES

        aprueba_acompanantes_id = "chkApruebaAcomp"

        driver.find_element_by_id(aprueba_acompanantes_id).click()
        time.sleep(3)

        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 2----CUANTOS ACOMPAÑANTES

        cuantos_acompanantes_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div[2]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(cuantos_acompanantes_xpath).click()
        time.sleep(3)

        #Observacion
        
        driver.find_element_by_xpath("//div[@method='get']//div[1]//div[1]//textarea[1]").send_keys("Derivacion Medica Programada - Auditar Medicamente")

        time.sleep(5)

        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 2

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()
        time.sleep(5)
        
        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Auditar Medicamente-----PASO 3

        #Prestaciones Aprobadas------Descripción

        descripcion_id = "prestacionDescripcionA"

        driver.find_element_by_id(descripcion_id).send_keys("odontologia")
        time.sleep(5)

        #Prestaciones Aprobadas------Descripción---LUPA

        lupa_id2 = "searchAprobadas"

        driver.find_element_by_id(lupa_id2).click()
        time.sleep(5)

        #Prestaciones Aprobadas------Descripción---LUPA-----Busqueda de Prestaciones

        prestacion_xpath = "//a[normalize-space()='993503']"

        driver.find_element_by_xpath(prestacion_xpath).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Auditar Medicamente----CONTINUAR

        continuar_id = "1"

        driver.find_element_by_id(continuar_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Auditar Medicamente----FLECHA

        flecha_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath).click()
        time.sleep(10)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)          

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']") 

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE       
             
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(5)  

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\06-Gestionar_Turno\\GestionarTurno_3-1.png")      
        time.sleep(5)

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8)   
        
        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #Derivacion Medica Programada - Gestionar Turno----OBSERVACION

        observacion_dmp = "observacionDMP"

        driver.find_element_by_id(observacion_dmp).send_keys("Derivacion Medica Programada - Gestionar Turno")
        time.sleep(5)

        #Derivacion Medica Programada - Gestionar Turno-----AGREGAR PRESTADOR

        lupa_prestador_id = "btnBuscarPrestador"

        driver.find_element_by_id(lupa_prestador_id).click()
        time.sleep(2)

        #Derivacion Medica Programada - Gestionar Turno-----POP UP BUSCAR PRESTADOR 

        codigo_id2 = "codigoBusqueda" #1130

        driver.find_element_by_id(codigo_id2).send_keys("1130")
        time.sleep(2)

        #Derivacion Medica Programada - Gestionar Turno-----POP UP LUPA BUSCAR 

        lupa_prestador_id2 = "btnBusquedaPrestadorBuscar"

        driver.find_element_by_id(lupa_prestador_id2).click()
        time.sleep(2)

        #Derivacion Medica Programada - Gestionar Turno-----POP UP SELECCIONAR CODIGO

        codigo_xpath = "//a[@title='Click para seleccionar']"

        driver.find_element_by_xpath(codigo_xpath).click()
        time.sleep(4)

        #Derivacion Medica Programada - Gestionar Turno-----POP UP SELECCIONAR LUGAR DE ATENCION

        calle_xpath = "//a[normalize-space()='TTE GRAL JUAN D PERON']"

        driver.find_element_by_xpath(calle_xpath).click()
        time.sleep(4)

        #Derivacion Medica Programada - Gestionar Turno-----SWICHT SELECCIONE PRESTADOR

        prestador_swith_id = "chkSelectedPrestador"

        driver.find_element_by_id(prestador_swith_id).click()
        time.sleep(4)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 1

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 2----FECHA

        #------METODO PARA FECHA------------  

        base = datetime.date.today()

        fechahoy = base.strftime("%d/%m/%Y")

        #Fecha y Horario del Turno 

        fecha_xpath4= "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[1]/input[1]"
        hora_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/span[1]/i[1]"
        flecha_hora_xpath = "//a[@data-action='incrementHour']"

        driver.find_element_by_xpath(fecha_xpath4).send_keys(fechahoy)
        time.sleep(2)

        driver.find_element_by_xpath(hora_xpath).click()
        driver.find_element_by_xpath(flecha_hora_xpath).click()
        time.sleep(2)

        #BOTON AGREGAR FECHA Y HORARIO 

        mas_xpath = "//form[@method='get']//div//div//div//div//div//div//div//div//div//div//div//div//div//button[@onclick='return false;']"

        driver.find_element_by_xpath(mas_xpath).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 2

        finalizar_id = "btnContinuar"

        driver.find_element_by_id(finalizar_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 2----NECESITA TRASLADO

        traslado_id = "necesitaTraslado"

        driver.find_element_by_id(traslado_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 2----NECESITA ALOJAMIENTO

        alojamiento_id = "necesitaAlojamiento"

        driver.find_element_by_id(alojamiento_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno-----PASO 3

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys(" Derivacion Medica Programada - Gestionar Turno")
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno ----CONTINUAR

        continuar_id = "1"

        driver.find_element_by_id(continuar_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno----FLECHA

        flecha_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath).click()
        time.sleep(2)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)    

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_xpath("//i[@data-placement='left']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE
                  
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()

        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\07-Gestionar_Autorizacion\\GestionarAutorizacion_1.png")
        time.sleep(5) 

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8)   

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #--------Derivacion Medica Programada - Gestionar Autorizacion        

        #--------Derivacion Medica Programada - Observacion

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys(" Derivacion Medica Programada - Gestionar Autorizacion")
        time.sleep(3)

        #-------- Derivacion Medica Programada - Gestionar Turno ----CONTINUAR

        continuar_id = "1"

        driver.find_element_by_id(continuar_id).click()
        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Turno----FLECHA

        flecha_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath).click()
        time.sleep(2)    

        #--------BUSQUEDA INTELIGENTE TRAMITE         

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                            

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_xpath("//i[@data-placement='left']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE
                  
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()

        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\08-Gestionar_Firma\\GestionarFirma_1.png")
        time.sleep(5) 

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8)  

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #--------Derivacion Medica Programada - Gestionar Firma Formulario

        #NOMBRE -- APELLIDO ---

        nombre_id = "firmarFormNombre"

        driver.find_element_by_id(nombre_id).send_keys("Prueba")

        time.sleep(2)

        apellido_id = "firmarFormApellido"

        driver.find_element_by_id(apellido_id).send_keys("Prueba2")

        time.sleep(2)

        agregar_id = "btnAgregarTelFirmarForm"

        driver.find_element_by_id(agregar_id).click()

        time.sleep(2)
        
        #POP UP Nuevo Teléfono---PROVINCIA

        provincia_xpath = "//form[@method='get']//div//div//div//div//div//div//div//div//option[@value='1']"

        driver.find_element_by_xpath(provincia_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---PARTIDO

        partido_xpath = "//form[@method='get']//div//div[2]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(partido_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---LOCALIDAD

        localidad_xpath = "//form[@method='get']//div//div//div//div//div//div[3]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(localidad_xpath).click()
        time.sleep(5)

        #POP UP Nuevo Teléfono---NUMERO

        numero_id2= "firmarFormularioPhonenumero"

        driver.find_element_by_id(numero_id2).send_keys("1123938926")
        time.sleep(5)

        #POP UP Nuevo Teléfono---VALIDAR

        validar_id = "firmarFormularioPhonevalidatePhoneButton"

        driver.find_element_by_id(validar_id).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---CONFIRMAR        
        
        confirmar_id = "phoneConfirmButton"

        driver.find_element_by_id(confirmar_id).click()
        time.sleep(4)
        
        #Email

        email_xpath = "//option[@value='otro']"  

        driver.find_element_by_xpath(email_xpath).click()
        
        campo_email_id = "inputFirmarFormEmail"

        driver.find_element_by_id(campo_email_id).send_keys("prueba@gmail.com")

        time.sleep(5)

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----Observacion

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys(" Derivacion Medica Programada - Gestionar Firma")

        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----ADJUNTAR DOCUMENTACION

        #ADJUNTAR DOCUMENTO-------ARCHIVO ADICIONAL

        adjuntar_xpath = "//button[@data-original-title='Ver y adjuntar archivos']//i"

        driver.find_element_by_xpath(adjuntar_xpath).click()
        time.sleep(2)  

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\prueba")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(5)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1)  

        action.key_down(Keys.ENTER)        

        time.sleep(3)       

        action.perform()

        time.sleep(3) 

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----POP UP OK        

        boton_ok_xpath = "//a[contains(text(),'OK')]"

        driver.find_element_by_xpath(boton_ok_xpath).click()
        time.sleep(5)

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----

        #DERIVAR GESTIONAR TRASLADO Y ALOJAMIENTO

        socio_id = "1"

        driver.find_element_by_id(socio_id).click()

        time.sleep(5)

        #DERIVAR GESTIONAR TRASLADO Y ALOJAMIENTO---FLECHA

        flecha_xpath2 = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath2).click()

        time.sleep(8)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)    
                                   

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE       
            
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\09-Gestionar_TyA\\GestionarTrasladoAlojamiento_1.png")
        time.sleep(5)

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8)        

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #Derivacion Medica Programada - Gestionar Traslado y Alojamiento----ORIGEN

        origen_id = "origen"

        driver.find_element_by_id(origen_id).send_keys("aaaa")
        time.sleep(3)

        #Derivacion Medica Programada - Gestionar Traslado y Alojamiento----DESTINO

        destino_id = "destino"

        driver.find_element_by_id(destino_id).send_keys("aaaa")
        time.sleep(3)

        #Derivacion Medica Programada - Gestionar Traslado y Alojamiento----FECHA

        #------METODO PARA FECHA------------  

        base = datetime.date.today()

        fechahoy = base.strftime("%d/%m/%Y")

        print(fechahoy)

        base_dias = base + datetime.timedelta(days=3)

        fecha_3dias = base_dias.strftime("%d/%m/%Y")

        print(fecha_3dias)

        #---------FECHA DE HOY + 3 DÍAS
             
        fechatraslado_id = "fechaDesdeTraslado"

        driver.find_element_by_id(fechatraslado_id).send_keys(fechahoy)

        time.sleep(3)

        fecha_hasta_traslado_id = "fechaHastaTraslado"

        driver.find_element_by_id(fecha_hasta_traslado_id).send_keys(fecha_3dias)

        time.sleep(3) 

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----IMPORTE

        importe_id ="importeTraslado"

        driver.find_element_by_id(importe_id).send_keys("1000")

        time.sleep(3) 

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----TIPO DE PENSION

        tipo_pension_id = "tipoPension"

        driver.find_element_by_id(tipo_pension_id).send_keys("aaaa")

        time.sleep(3)

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----TIPO DE HABITACION

        tipo_habitacion_id = "tipoHabitacion"

        driver.find_element_by_id(tipo_habitacion_id).send_keys("aaaa")

        time.sleep(3)

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----Alojamiento

        fechaalojamiento_id = "fechaDesdeAlojamiento"

        driver.find_element_by_id(fechaalojamiento_id).send_keys(fechahoy)

        time.sleep(5)

        fecha_hasta_alojamiento_id = "fechaHastaAlojamiento"

        driver.find_element_by_id(fecha_hasta_alojamiento_id).send_keys(fecha_3dias)

        time.sleep(5) 

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----ENVIAR NOTIFICACION

        enviar_notificacion_id = "enviarNotificacionTravel"

        driver.find_element_by_id(enviar_notificacion_id).click()

        time.sleep(5) 

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----Observacion

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Gestionar Traslado y Alojamiento")

        time.sleep(2)

        #FINALIZAR TRAMITE 

        finalizar_id = "1"

        driver.find_element_by_id(finalizar_id).click()

        time.sleep(2)

        #DERIVAR ---FLECHA

        flecha_xpath3 = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath3).click()

        time.sleep(5)        

        #--------BUSQUEDA INTELIGENTE TRAMITE       

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)   

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                                     

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE       
            
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\10-Esperar_Datos\\EsperarDatosViaje_1.png")
        time.sleep(5)

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8)        

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #-----------Derivacion Medica Programada - Esperar Datos Viaje

        #----------Derivacion Medica Programada - Esperar Datos Viaje----Valor del Traslado

        valor_traslado_id = "edv-valorTraslado"

        driver.find_element_by_id(valor_traslado_id).send_keys("1000")

        time.sleep(2)

        #----------Derivacion Medica Programada - Esperar Datos Viaje----Valor del Alojamiento

        valor_alojamiento_id = "edv-valorAlojamiento"

        driver.find_element_by_id(valor_alojamiento_id).send_keys("1000")

        time.sleep(2)

        #----------Derivacion Medica Programada - Esperar Datos Viaje----Observaciones

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Esperar Datos Viaje")

        time.sleep(2)

         #FINALIZAR TRAMITE 

        finalizar_id = "1"

        driver.find_element_by_id(finalizar_id).click()

        time.sleep(2)

        #DERIVAR ---FLECHA

        flecha_id = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha_id).click()
        time.sleep(8) 

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)          

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                     

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE       
            
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\11-Contactar_Socio\\ContactarSocio_1.png")
        time.sleep(5)

        driver.find_element_by_xpath(tomar_abrir_tramite_xpath).click()
        time.sleep(8) 

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #Derivacion Medica Programada - Contactar Socio---Validación de pantalla de la tarea "Contactar al socio"---Con soc - 003
        
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\11-Contactar_Socio\\ContactarSocio_3.png")

        #Derivacion Medica Programada - Contactar Socio---Validación de ingreso de caracteres en el campo "observación---Con soc - 004

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Contactar Socio1234567890")

        time.sleep(2)
        
        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\11-Contactar_Socio\\ContactarSocio_4.png")
                       
        #Derivacion Medica Programada - Contactar Socio---Validación del switch "Se contactó"---Con soc - 005

        contacto_socio_id = "contactoSocio"

        driver.find_element_by_id(contacto_socio_id).click()

        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\11-Contactar_Socio\\ContactarSocio_5.png")
      
        #FINALIZAR TRAMITE ---Validación del campo tipo combo "Seleccione una acción"---Con soc - 006

        accion_id = "actions"

        driver.find_element_by_id(accion_id).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\11-Contactar_Socio\\ContactarSocio_6.png")

        #FALTA COPIAR Y PEGAR LOS CP DE ADJUNTAR QUE SON PARECIDOS A LAS BANDEJAS ANTERIORES----CP 08 AL 18

        finalizar_id = "1"

        driver.find_element_by_id(finalizar_id).click()

        time.sleep(2)

        #DERIVAR CONTACTAR SOCIO---FLECHA

        flecha_xpath3 = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath3).click()

        time.sleep(5)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                     

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_1\\ETE\\Cierre_tramite.png")
        time.sleep(5)  


if __name__ == '__main__':
    unittest.main()