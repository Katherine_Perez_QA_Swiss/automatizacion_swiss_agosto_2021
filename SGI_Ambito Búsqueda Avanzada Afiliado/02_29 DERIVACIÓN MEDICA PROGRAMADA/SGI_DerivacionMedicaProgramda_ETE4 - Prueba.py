import unittest
from unittest.main import main
from selenium import webdriver
import time
import autoit
from selenium.webdriver.common.keys import Keys
import datetime

class DerivacionMedica (unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

        #ACCESO A SGI 

        driver = self.driver

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"

        driver.maximize_window()
        driver.get(url)
        time.sleep(5)

        #DATOS DE USUARIO 

        usuario = "EXT_MaFernan"
        contrasena = "u2y9fXxch"
        username_id = "userName"
        password_id = "password"
        disable_id = "disable"

        driver.find_element_by_id(username_id).send_keys(usuario)
        driver.find_element_by_id(password_id).send_keys(contrasena)
        driver.find_element_by_id(disable_id).click()

        time.sleep(15)              
    
    def test01_Derivacion(self):
        
        driver = self.driver        

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                            

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_xpath("//i[@data-placement='left']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE
                  
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()

        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE4\\8_GestionarFirma.png")
        time.sleep(5) 

        driver.find_element_by_xpath("//a[@data-original-title='Abrir']//i").click()
        time.sleep(3)    

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #--------Derivacion Medica Programada - Gestionar Firma Formulario

        #NOMBRE -- APELLIDO ---

        nombre_id = "firmarFormNombre"

        driver.find_element_by_id(nombre_id).send_keys("Prueba")

        time.sleep(2)

        apellido_id = "firmarFormApellido"

        driver.find_element_by_id(apellido_id).send_keys("Prueba2")

        time.sleep(2)

        agregar_id = "btnAgregarTelFirmarForm"

        driver.find_element_by_id(agregar_id).click()

        time.sleep(2)
        
        #POP UP Nuevo Teléfono---PROVINCIA

        provincia_xpath = "//form[@method='get']//div//div//div//div//div//div//div//div//option[@value='1']"

        driver.find_element_by_xpath(provincia_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---PARTIDO

        partido_xpath = "//form[@method='get']//div//div[2]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(partido_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---LOCALIDAD

        localidad_xpath = "//form[@method='get']//div//div//div//div//div//div[3]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(localidad_xpath).click()
        time.sleep(5)

        #POP UP Nuevo Teléfono---NUMERO

        numero_id2= "firmarFormularioPhonenumero"

        driver.find_element_by_id(numero_id2).send_keys("1123938926")
        time.sleep(5)

        #POP UP Nuevo Teléfono---VALIDAR

        validar_id = "firmarFormularioPhonevalidatePhoneButton"

        driver.find_element_by_id(validar_id).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---CONFIRMAR        
        
        confirmar_id = "phoneConfirmButton"

        driver.find_element_by_id(confirmar_id).click()
        time.sleep(4)
        
        #Email

        email_xpath = "//option[@value='katherine.perez@qactions.com']"  

        driver.find_element_by_xpath(email_xpath).click()

        time.sleep(5)

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----Observacion

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys(" Derivacion Medica Programada - Gestionar Firma")

        time.sleep(2)

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----ADJUNTAR DOCUMENTACION

        #ADJUNTAR DOCUMENTO-------ARCHIVO ADICIONAL

        adjuntar_xpath = "//button[@data-original-title='Ver y adjuntar archivos']//i"

        driver.find_element_by_xpath(adjuntar_xpath).click()
        time.sleep(2)  

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\prueba")

        #autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\prueba2")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(5)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(3) 

        action.key_up(Keys.TAB)

        time.sleep(3) 

        action.key_down(Keys.TAB)

        time.sleep(3) 

        action.key_up(Keys.TAB)

        time.sleep(3)

        action.key_down(Keys.TAB)

        time.sleep(3) 

        action.key_up(Keys.TAB)

        time.sleep(3)

        action.key_down(Keys.TAB)

        time.sleep(3) 

        action.key_up(Keys.TAB)

        time.sleep(3)

        action.key_down(Keys.ENTER)        

        time.sleep(3)

        action.key_up(Keys.ENTER)        

        time.sleep(3)

        action.perform()

        time.sleep(3) 

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----POP UP OK        

        boton_ok_xpath = "//a[contains(text(),'OK')]"

        driver.find_element_by_xpath(boton_ok_xpath).click()
        time.sleep(5)

        #-------- Derivacion Medica Programada - Gestionar Firma Formulario-----

        #DERIVAR GESTIONAR TRASLADO Y ALOJAMIENTO

        socio_id = "1"

        driver.find_element_by_id(socio_id).click()

        time.sleep(5)

        #DERIVAR GESTIONAR TRASLADO Y ALOJAMIENTO---FLECHA

        flecha_xpath2 = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath2).click()

        time.sleep(2)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)        
                                   

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE       
            
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE4\\9_GestionarTrasladoAlojamiento.png")
        time.sleep(5)

        driver.find_element_by_xpath("//a[@data-original-title='Tomar y Abrir']//i").click()
        time.sleep(3)         

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #Derivacion Medica Programada - Gestionar Traslado y Alojamiento----ORIGEN

        origen_id = "origen"

        driver.find_element_by_id(origen_id).send_keys("aaaa")
        time.sleep(3)

        #Derivacion Medica Programada - Gestionar Traslado y Alojamiento----DESTINO

        destino_id = "destino"

        driver.find_element_by_id(destino_id).send_keys("aaaa")
        time.sleep(3)

        #Derivacion Medica Programada - Gestionar Traslado y Alojamiento----FECHA

        #------METODO PARA FECHA------------  

        base = datetime.date.today()

        fechahoy = base.strftime("%d/%m/%Y")

        print(fechahoy)

        base_dias = base + datetime.timedelta(days=3)

        fecha_3dias = base_dias.strftime("%d/%m/%Y")

        print(fecha_3dias)

        #---------FECHA DE HOY + 3 DÍAS
             
        fechatraslado_id = "fechaDesdeTraslado"

        driver.find_element_by_id(fechatraslado_id).send_keys(fechahoy)

        time.sleep(3)

        fecha_hasta_traslado_id = "fechaHastaTraslado"

        driver.find_element_by_id(fecha_hasta_traslado_id).send_keys(fecha_3dias)

        time.sleep(3) 

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----IMPORTE

        importe_id ="importeTraslado"

        driver.find_element_by_id(importe_id).send_keys("1000")

        time.sleep(3) 

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----TIPO DE PENSION

        tipo_pension_id = "tipoPension"

        driver.find_element_by_id(tipo_pension_id).send_keys("aaaa")

        time.sleep(3)

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----TIPO DE HABITACION

        tipo_habitacion_id = "tipoHabitacion"

        driver.find_element_by_id(tipo_habitacion_id).send_keys("aaaa")

        time.sleep(3)

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----Alojamiento

        fechaalojamiento_id = "fechaDesdeAlojamiento"

        driver.find_element_by_id(fechaalojamiento_id).send_keys(fechahoy)

        time.sleep(5)

        fecha_hasta_alojamiento_id = "fechaHastaAlojamiento"

        driver.find_element_by_id(fecha_hasta_alojamiento_id).send_keys(fecha_3dias)

        time.sleep(5) 

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----ENVIAR NOTIFICACION

        enviar_notificacion_id = "enviarNotificacionTravel"

        driver.find_element_by_id(enviar_notificacion_id).click()

        time.sleep(5) 

        #---------Derivacion Medica Programada - Gestionar Traslado y Alojamiento----Observacion

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Gestionar Traslado y Alojamiento")

        time.sleep(2)

        #FINALIZAR TRAMITE 

        finalizar_id = "1"

        driver.find_element_by_id(finalizar_id).click()

        time.sleep(2)

        #DERIVAR ---FLECHA

        flecha_xpath3 = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath3).click()

        time.sleep(5)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)   

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                                     

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE       
            
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE4\\10_EsperarDatosViaje.png")
        time.sleep(5)

        driver.find_element_by_xpath("//a[@data-original-title='Tomar y Abrir']//i").click()
        time.sleep(3)         

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #-----------Derivacion Medica Programada - Esperar Datos Viaje

        #----------Derivacion Medica Programada - Esperar Datos Viaje----Valor del Traslado

        valor_traslado_id = "edv-valorTraslado"

        driver.find_element_by_id(valor_traslado_id).send_keys("1000")

        time.sleep(2)

        #----------Derivacion Medica Programada - Esperar Datos Viaje----Valor del Alojamiento

        valor_alojamiento_id = "edv-valorAlojamiento"

        driver.find_element_by_id(valor_alojamiento_id).send_keys("1000")

        time.sleep(2)

        #----------Derivacion Medica Programada - Esperar Datos Viaje----Observaciones

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Esperar Datos Viaje")

        time.sleep(2)

         #FINALIZAR TRAMITE 

        finalizar_id = "1"

        driver.find_element_by_id(finalizar_id).click()

        time.sleep(2)

        #DERIVAR ---FLECHA

        flecha_xpath3 = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath3).click()

        time.sleep(5)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)          

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                     

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        #--------VALIDACIÓN PARA INGRESO DE TRAMITE       
            
        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3) 

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE4\\11_ContactarSocio.png")
        time.sleep(5)

        driver.find_element_by_xpath("//a[@data-original-title='Tomar y Abrir']//i").click()
        time.sleep(15) 

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #Derivacion Medica Programada - Contactar Socio

        #Derivacion Medica Programada - Contactar Socio---SWICTH

        contacto_socio_id = "contactoSocio"

        driver.find_element_by_id(contacto_socio_id).click()

        time.sleep(2)

        #Derivacion Medica Programada - Contactar Socio---OBSERVACION

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Contactar Socio")

        time.sleep(2)

        #FINALIZAR TRAMITE 

        finalizar_id = "1"

        driver.find_element_by_id(finalizar_id).click()

        time.sleep(2)

        #DERIVAR CONTACTAR SOCIO---FLECHA

        flecha_xpath3 = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath3).click()

        time.sleep(5)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)  

        driver.find_element_by_id("inputAdvanceSearch").send_keys("#252884670")

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)                     

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_ETE\\ETE4\\12_cierretramite.png")
        time.sleep(5)  


if __name__ == '__main__':
    unittest.main()