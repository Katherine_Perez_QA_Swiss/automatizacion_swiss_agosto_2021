import unittest
from selenium import webdriver
import time
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


class Agradecimiento(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")
    
    def test_agradecimiento(self):

        #INSTRUCCIONES PARA ABRIR VENTANA SGI
        
        #URL

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"        
        
        driver = self.driver
        driver.maximize_window()
        time.sleep(4)   
        driver.get(url)  
        titulopage = driver.title

		# verify title is SGI

        assertEqual = self.assertEqual
        assertEqual("SGI", titulopage, "No pudo acceder a SGI")     
        time.sleep(6)

        #INGRESO DE USUARIOS

        usuario = "ext_roperalt"
        contrasena = "Qactions01"
        username = "userName"
        password = "password"
        disable = "disable"

        driver.find_element_by_id(username).send_keys(usuario)
        titulo_Sgi = driver.find_element_by_id("userName").text
        print(titulo_Sgi)
        driver.find_element_by_id(password).send_keys(contrasena)
        driver.find_element_by_id(disable).click()
        time.sleep(4)     
        time.sleep(6)

        # verify el correcto ingreso a SGI
        
        sgi = "logoImg"
        assertEqual(sgi, sgi, "No pudo acceder a SGI")
        
        #BUSCAR

        buscar = "//a[@id='searchTypeEntity']//span[@class='menu-text']"
        
        driver.find_element_by_xpath(buscar).click()
        time.sleep(8)   

        # verify el correcto ingreso a busqueda 

        avanzadaafiliado = "//body//div//div//div//div//div//div//div//div//div//b[1]" 

        assertEqual(avanzadaafiliado, avanzadaafiliado, "No avanza a la busqueda de afiliado")     
        
        #INGRESO DE DU

        DU = "29197733"
        afiliado = "txtAfiliadoDocumento"
        boton = "btnSearch2"

        driver.find_element_by_id(afiliado).send_keys(DU)
        driver.find_element_by_id(boton).click()
        time.sleep(6)

        # verify el correcto ingreso al afiliado

        datosafiliado = "//a[normalize-space()='Datos Del Afiliado']"
        assertEqual(datosafiliado, datosafiliado, "No avanza a la busqueda de afiliado")     
        time.sleep(6)

        #MOVER HASTA LA RUEDA

        buscarueda = driver.find_element_by_css_selector("#ace-settings-btn")
        webdriver.ActionChains(driver).click_and_hold(buscarueda).perform()
        time.sleep(4)                     

        #SELECCIONAR PROCESO

        proceso = "inputProcess"
        nombre = "AGRA"
        numero = "41"       
        
        driver.find_element_by_id(proceso).send_keys(nombre)
        time.sleep(2)

        assertEqual(numero, numero, "No se encuentra el proceso")   

        #EVIDENCIA DEL CP1        

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP1.png")
        time.sleep(2)  

        driver.find_element_by_id(numero).click()
        time.sleep(3)        

        #ACCEDER AL FRAME

        driver.switch_to.frame(driver.find_element_by_id("my_frame41"))
        time.sleep(6)

        #GESTION AGRADECIMIENTO

        #PANTALLA DE AGRADECIMIENTO

        agradecimiento = "41-link-tab"

        assertEqual(agradecimiento, agradecimiento, "No se encuentra el proceso") 

        #EVIDENCIA DEL CP2

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP2.png")
        time.sleep(2) 

        #CAMPO OBSERVACION OBLIGATORIO

        grabar = "btnSave"

        driver.find_element_by_id(grabar).click()
        time.sleep(2)

        mensaje = "//span[@for='observacion']"

        assertEqual(mensaje, mensaje, "No se considera campo obligatorio") 

        #EVIDENCIA DEL CP3

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP3.png")
        time.sleep(2) 

        #AGREGAR DATOS AL CAMPO OBSERVACIÓN

        observacion = driver.find_element_by_id("observacion")        
        
        observacion.send_keys("12345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678123456781234567812345678")

        time.sleep(4)

        observacion.clear()

        time.sleep(4)   

        observacion.send_keys("Agradecimiento")

        time.sleep(4)       
       
        #texto = driver.find_element_by_class_name("span12 limited", value = "800").text

        #print(texto) 
       
        #EVIDENCIA DEL CP8

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP8.png")
        time.sleep(2) 

        time.sleep(4)

        #LISTA DESPLEGABLE COMO OBLIGATORIO

        driver.find_element_by_id(grabar).click()
        time.sleep(2)

        mensaje2 = "//span[@for='submotivo']"

        assertEqual(mensaje2, mensaje2, "No se considera obligatorio") 

        #EVIDENCIA DEL CP4

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP4.png")
        time.sleep(2) 

        #SELECCIONAR MOTIVO EN LISTA DESPLEGABLE

        motivo =  "submotivo" 
        lista = ["//option[@value='10']","//option[@value='75']"]

        driver.find_element_by_id(motivo).click()
        driver.find_element_by_xpath(lista[1]).click()
        time.sleep(2)

        #validar atributo como obligatorio

        driver.find_element_by_id(grabar).click()
        time.sleep(2)

        mensaje3= "//span[@for='atributo']"

        assertEqual(mensaje3, mensaje3, "No se considera obligatorio") 

        #EVIDENCIA DEL CP9 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\Login\\ProyectoSGI\\Python Scripts\\Agradecimiento\\CP5.png")
        time.sleep(2)  


        #Relacionar tramite 

        """
        relacionar = "btnViewEntityCalls"

        driver.find_element_by_id(relacionar).click()
        time.sleep(8)

        tramite = "//a[@value='207461083']//i"

        driver.find_element_by_xpath(tramite).click()
        time.sleep(15)

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP10.png")
        time.sleep(2) 

        """

        # Grabar un Tramite con  Altas y bajas

        driver.find_element_by_id(motivo).click()
        driver.find_element_by_xpath(lista[0]).click()
        time.sleep(2)

        driver.find_element_by_id(grabar).click()
        time.sleep(2)

        #CAPTURA DE PANTALLA GENERACIÓN TRAMITE CP6

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP6.png")
        time.sleep(4)           

        #TOMAR TRÁMITE

        numerotramite = driver.find_element_by_css_selector("div[class='modal-body']").text

        tramite = "#" + numerotramite[31:40]

        print("Impresión de"+ tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(5)

        #BUSQUEDA INTELIGENTE DE TRAMITE 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)

        #GESTION TRAMITE

        lupa =  driver.find_element_by_id("advanceSearch")
        
        mover = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,mover).perform()
        time.sleep(5)

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(5)

        #INGRESAR IFRAME TASK
        
        iframe = "iframeTask"
        
        driver.switch_to.frame(driver.find_element_by_id(iframe))
        time.sleep(6)

        #VERIFICAR PANTALLA DE GESTION DE AGRADECIMIENTO

        assertEqual(iframe, iframe, "No se logra acceder a gestion Agradecimiento") 
        
        accion = driver.find_element_by_id("actions")
        webdriver.ActionChains(driver).click_and_hold(accion).perform()
        time.sleep(3)     

        #EVIDENCIA DEL CP9         

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP9.png")
        time.sleep(3)   

        #MODIFICAR CAMPO DE OBSERVACION

        observacion = driver.find_element_by_id("observacion")

        observacion.clear()

        time.sleep(4)   

        observacion.send_keys("Cambio de Observación Agradecimiento")

        time.sleep(4)  

        #EVIDENCIA DEL CP11        

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP11.png")
        time.sleep(3)    

        #MODIFICAR MOTIVO 
       
        driver.find_element_by_id(motivo).click()
        driver.find_element_by_xpath(lista[1]).click()
        time.sleep(2)

        driver.find_element_by_id("atributo").click()
        driver.find_element_by_xpath("//*[@id='atributo']/option[2]").click()

        time.sleep(2)

        #EVIDENCIA DEL CP12        

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP12.png")
        time.sleep(3) 

        #FINALIZAR EL TRAMITE

        driver.find_element_by_id("actions").click()

        time.sleep(2)

        finalizar = "//option[@id='1']" 
        boton2 = "btnSaveAndEndTask"

        driver.find_element_by_xpath(finalizar).click()
        time.sleep(2)

        driver.find_element_by_id(boton2).click()
        time.sleep(2) 

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(10)

        #EVIDENCIA DEL CP13

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_64 AGRADECIMIENTO\\Evidencia_CP\\CP13.png")
        time.sleep(3) 

if __name__ == '__main__':
    unittest.main()






