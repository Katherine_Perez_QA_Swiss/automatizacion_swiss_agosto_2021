import unittest
from unittest import main
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys

class Certificado_Antiguedad(unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

    def test_Certificado(self):

        driver = self.driver

        #INGRESO A SGI

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"

        driver.maximize_window()
        driver.get(url2)
        time.sleep(5)

        #INGRESO DE USUARIOS

        usuario = "ext_roperalt"
        contrasena = "Qactions01"
        username = "userName"
        password = "password"
        disable = "disable"

        driver.find_element_by_id(username).send_keys(usuario)
        driver.find_element_by_id(password).send_keys(contrasena)
        driver.find_element_by_id(disable).click()
        time.sleep(4)        
        
        #BUSCAR PROCESO

        buscar = "//a[@id='searchTypeEntity']//span[@class='menu-text']"
        
        driver.find_element_by_xpath(buscar).click()
        time.sleep(10)

        #INGRESO DE DU

        DU = "45822651"
        afiliado = "txtAfiliadoDocumento"
        boton = "btnSearch2"

        driver.find_element_by_id(afiliado).send_keys(DU)
        driver.find_element_by_id(boton).click()

        time.sleep(15)

        #MOVER HASTA LA RUEDA

        buscarueda = driver.find_element_by_css_selector("#ace-settings-btn")
        webdriver.ActionChains(driver).click_and_hold(buscarueda).perform()
        time.sleep(4)

        #SELECCIONAR UN PROCESO

        proceso = "inputProcess"
        nombre = "Certi"
        numero = "23"
        
        driver.find_element_by_id(proceso).send_keys(nombre)
        time.sleep(2)
        driver.find_element_by_id(numero).click()
        time.sleep(3)

        #ACCEDER AL IFRAME PROCESO

        iframe = "my_frame23" 

        driver.switch_to.frame(driver.find_element_by_id(iframe))
        time.sleep(10) 

        #GESTION CERTIFICADO ANTIGUEDAD

        #CAMPO OBSERVACION-----PASO 1

        observacion_id = "observacion"
        siguiente_button_xpath = "//button[contains(@data-last,'Finalizar')]" 

        driver.find_element_by_id(observacion_id).send_keys("Certificado de Antiguedad")
        time.sleep(2)
        driver.find_element_by_xpath(siguiente_button_xpath).click()
        time.sleep(5)

        #DATOS DE COBERTURA-----PASO 2

        #CAPTURE PANTALLA ----CERTIFICADO        

        firma_xpath = driver.find_element_by_xpath("//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div[2]")
        webdriver.ActionChains(driver).move_to_element(firma_xpath).perform()
        time.sleep(5)        
                  
        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_56 CERTIFICADO DE ANTIGUEDAD\\Evidencia_ETE\\certificado.png")
        time.sleep(2)

        driver.find_element_by_xpath(siguiente_button_xpath).click()
        time.sleep(2)

        #PANTALLA DE DATOS DE AFILIADO 

        #CAPTURE PANTALLA ----AFILIADO

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_56 CERTIFICADO DE ANTIGUEDAD\\Evidencia_ETE\\datosafiliado.png")
        time.sleep(2)

if __name__ == '__main__':
    unittest.main()
    





