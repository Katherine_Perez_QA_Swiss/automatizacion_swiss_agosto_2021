import unittest
from selenium import webdriver
import time

class Clientes_VIP(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

    def test_ClientesVIP(self):
        driver = self.driver

        #ACCESO A SGI

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"

        driver.maximize_window()
        driver.get(url)
        time.sleep(3)

        #INGRESO DE USUARIOS

        usuario = "ext_roperalt"
        contrasena = "Qactions01"
        username = "userName"
        password = "password"
        disable = "disable"

        driver.find_element_by_id(username).send_keys(usuario)
        driver.find_element_by_id(password).send_keys(contrasena)
        driver.find_element_by_id(disable).click()
        time.sleep(4)  

        #BUSCAR CLIENTE

        buscar = "//a[@id='searchTypeEntity']//span[@class='menu-text']"
        
        driver.find_element_by_xpath(buscar).click()
        time.sleep(10)

        #INGRESO DE CONTRATO VALIDO

        contrato_id = "txtAfiliadoContrato"
        numero_contrato = "0583791"
        buscar_button_id = "btnSearch2"
        
        driver.find_element_by_id(contrato_id).send_keys(numero_contrato)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(2)

        #SELECCIONAR AFILIADO EN LA GRILLA 

        afiliado_xpath = "//tbody/tr[2]/td[3]/a[1]"

        driver.find_element_by_xpath(afiliado_xpath).click()
        time.sleep(2)

        #MOVER HASTA LA RUEDA

        buscarueda = driver.find_element_by_css_selector("#ace-settings-btn")
        webdriver.ActionChains(driver).click_and_hold(buscarueda).perform()
        time.sleep(4)

        #SELECCIONAR UN PROCESO

        proceso_id = "inputProcess"
        nombre = "VIP"
        numero_id = "103"
        
        driver.find_element_by_id(proceso_id).send_keys(nombre)
        time.sleep(2)
        driver.find_element_by_id(numero_id).click()
        time.sleep(3)

        #ACCEDER AL IFRAME PROCESO

        iframe = "my_frame103" 

        driver.switch_to.frame(driver.find_element_by_id(iframe))
        time.sleep(10) 

        #GESTION PROCESO----LISTA DESPLEGABLE TIPO

        tipo_xpath = "//option[@value='1']"

        driver.find_element_by_xpath(tipo_xpath).click()
        time.sleep(2)

        #MANEJO DE GRILLA 

        marca_checkbox_id = "familyGroup0estado"
        observacion_id = "familyGroup0observacion"

        driver.find_element_by_id(marca_checkbox_id).click()
        driver.find_element_by_id(observacion_id).clear()
        driver.find_element_by_id(observacion_id).send_keys("Prueba VIP")
        time.sleep(2)

        #GRABAR TRAMITE 

        grabar_button_id = "btnSave"

        driver.find_element_by_id(grabar_button_id).click()
        time.sleep(2)

        #CAPTURA DE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02-111 CLIENTES VIP MARCAS ADICIONALES\\Evidencia_ETE\\tramiteVIP.png")
        time.sleep(3)

        #--------TOMAR TRAMITE

        numerotramite = driver.find_element_by_css_selector("div[class='bootbox modal fade in']>div[class='modal-body']").text

        tramite = "#" + numerotramite[31:40]

        print("Tramite numero" + tramite)

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)        

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_id("tagDateTasks")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURA DE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02-111 CLIENTES VIP MARCAS ADICIONALES\\Evidencia_ETE\\tramiteVIPcerrado.png")
        time.sleep(3)  

if __name__ == '__main__':
    unittest.main()
          
