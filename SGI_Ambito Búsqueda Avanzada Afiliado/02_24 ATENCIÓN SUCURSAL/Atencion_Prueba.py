import unittest
from unittest.main import main
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
#from webdriver_manager.chrome import ChromeDriverManager


class AtencionSucursal(unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")
        #self.driver = webdriver.Chrome(ChromeDriverManager().install())

    def test_Atencion(self):

        #ACCESO A SGI

        driver = self.driver
        driver.maximize_window()

        url = "http://sgipre/sgi-app/#login"
        url2= "http://sgiqa/sgi-app/#login"

        driver.get(url)
        time.sleep(7)

        #VERIFICAR EL TITULO DE SGI 

        titulopage = driver.title

        assertEqual = self.assertEqual

        assertEqual = ("SGI", titulopage, "No se logró acceder a SGI")

        #DATOS DE ACCESO SGI

        usuario = "EXT_MaFernan"
        contrasena = "u2y9fXxch"
        user_id = "userName"
        password_id = "password"
        ingresar_button = "disable"

        driver.find_element_by_id(user_id).send_keys(usuario)
        driver.find_element_by_id(password_id).send_keys(contrasena)
        driver.find_element_by_id(ingresar_button).click()
        time.sleep(3)        

        #--------CP 85

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "45822651"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)     

        #--------CP 99

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "45822651"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL -------------------

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"   
        recepcionar_2 = driver.find_element_by_xpath(recepcionar_xpath)

        action = webdriver.ActionChains(driver)

        action.click_and_hold(recepcionar_2)        

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP99.png")
        time.sleep(3)

        #--------CP 100

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = "//select[@onchange='submotivosChange(this);']"    

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(2)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP100.png")
        time.sleep(3)

        #--------CP 100

        ejecutivo_id = "ejecutivos"

        driver.find_element_by_id(ejecutivo_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP100.png")
        time.sleep(5)

        #--------CP 101

        contacto_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div//div[4]//div[1]//div[1]//div[1]//div[1]//select[1]"

        driver.find_element_by_xpath(contacto_xpath).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP101.png")
        time.sleep(5)

        #--------CP 102

        observacion_id = "obs-motivo-recepcion"

        driver.find_element_by_id(observacion_id).send_keys("Prueba123456789!?¡?¡?¡?¡")
        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP102.png")
        time.sleep(3)

        #--------CP 103

        socio_id = "chkPriority"

        driver.find_element_by_id(socio_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP103.png")
        time.sleep(3)

        #--------CP 104

        atender_id = "chkAttentionNow"

        driver.find_element_by_id(atender_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP104.png")
        time.sleep(3)

        #--------CP 105

        telefono_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='1']"
        tipo_telefono_id = "telContacto"

        driver.find_element_by_xpath(telefono_xpath).click()
        time.sleep(3)

        driver.find_element_by_id(tipo_telefono_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP105.png")
        time.sleep(3)

        #--------CP 107

        mail_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='3']"
        tipo_telefono_id = "mailContacto"

        driver.find_element_by_xpath(mail_xpath).click()
        time.sleep(3)

        driver.find_element_by_id(tipo_telefono_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP107.png")
        time.sleep(3)

        #--------CP 110

        cerrar_xpath = "//div//div//div//div//div//div[4]//div[1]//div[1]//div[1]//div[1]//span[1]//a[1]//i[1]"
        
        driver.find_element_by_xpath(cerrar_xpath).click()
        time.sleep(3)        

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP110.png")
        time.sleep(3)

if __name__ == '__main__':
    unittest.main()
    