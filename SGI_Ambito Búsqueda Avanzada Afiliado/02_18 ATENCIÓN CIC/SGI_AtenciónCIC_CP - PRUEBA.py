import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
import time 
import autoit
from selenium.webdriver.common.keys import Keys


class Usando_Unittest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(executable_path=r"C:\Users\QA-User61\geckodriver.exe")
        
    def test_ingresosgi(self):        
        
         #INSTRUCCIONES PARA ABRIR VENTANA SGI
        
        #URL

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"        
        
        driver = self.driver
        driver.maximize_window()
        time.sleep(4)   
        driver.get(url)  
        titulopage = driver.title

		# verify title is SGI

        assertEqual = self.assertEqual
        assertEqual("SGI", titulopage, "No pudo acceder a SGI")     
        time.sleep(6)

        #INGRESO DE USUARIOS

        usuario = "ext_roperalt"
        contrasena = "Qactions01"
        username = "userName"
        password = "password"
        disable = "disable"

        driver.find_element_by_id(username).send_keys(usuario)
        driver.find_element_by_id(password).send_keys(contrasena)
        driver.find_element_by_id(disable).click()
        time.sleep(4)     
        time.sleep(6)

        # verify el correcto ingreso a SGI
        
        sgi = "logoImg"
        assertEqual(sgi, sgi, "No pudo acceder a SGI")
        
        #BUSCAR

        buscar = "//a[@id='searchTypeEntity']//span[@class='menu-text']"
        
        driver.find_element_by_xpath(buscar).click()
        time.sleep(8)   

        # verify el correcto ingreso a busqueda 

        avanzadaafiliado = "//body//div//div//div//div//div//div//div//div//div//b[1]" 

        assertEqual(avanzadaafiliado, avanzadaafiliado, "No avanza a la busqueda de afiliado")     
        
        #INGRESO DE DU

        DU = "12270576"
        afiliado = "txtAfiliadoDocumento"
        boton = "btnSearch2"

        driver.find_element_by_id(afiliado).send_keys(DU)
        driver.find_element_by_id(boton).click()
        time.sleep(6)

        # verify el correcto ingreso al afiliado

        datosafiliado = "//a[normalize-space()='Datos Del Afiliado']"
        assertEqual(datosafiliado, datosafiliado, "No avanza a la busqueda de afiliado")     
        time.sleep(6)

        #MOVER HASTA LA RUEDA

        buscarueda = driver.find_element_by_css_selector("#ace-settings-btn")
        webdriver.ActionChains(driver).click_and_hold(buscarueda).perform()
        time.sleep(4)                     

        #SELECCIONAR PROCESO

        proceso = "inputProcess"
        nombre = "CIC"
        numero = "515"       
        
        driver.find_element_by_id(proceso).send_keys(nombre)
        time.sleep(2)

        assertEqual(numero, numero, "No se encuentra el proceso")   

        #EVIDENCIA DEL CP0        

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP0.png")
        time.sleep(6)  

        driver.find_element_by_id(numero).click()
        time.sleep(3) 

        #------ACCEDER AL FRAME------------

        iframe = "my_frame515" 

        driver.switch_to.frame(driver.find_element_by_id(iframe))
        time.sleep(2)

        #ADJUNTAR DOCUMENTO

        adjuntar_xpath = "//button[@onclick='return false;']//i"

        driver.find_element_by_xpath(adjuntar_xpath).click()
        time.sleep(2)

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\maty")        

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(10)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)

        #PANTALLA DE AGRADECIMIENTO

        atencioncic = "515-link-tab"

        assertEqual(atencioncic, atencioncic, "No se encuentra el proceso") 

        #EVIDENCIA DEL CP1       

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP1.png")
        time.sleep(6)     

        #CAMPO DE OBSERVACIONES 

        observacion = "atencionCIC[detalle][observacion]"

        buscarobservacion = driver.find_element_by_name(observacion)
        webdriver.ActionChains(driver).click_and_hold(buscarobservacion).perform()
        time.sleep(4)       

        assertEqual(observacion, observacion, "No se encuentra el campo") 

        #EVIDENCIA DEL CP2       

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP2.png")
        time.sleep(6) 

        #VALIDAR CAMPO DE OBSERVACION OBLIGATORIO

        seleccion = "option[value='21']"   
             
        grabar = "btnSave"

        driver.find_element_by_css_selector(seleccion).click() 
        driver.find_element_by_id(grabar).click()
        time.sleep(3)

        mensaje = "//body//div//div//div//div//p"

        assertEqual(mensaje, mensaje, "No se considera campo obligatorio") 

        #EVIDENCIA DEL CP3 y CP4    

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP3yCP4.png")
        time.sleep(6) 

        #RELACIONAR TRAMITE 

        #VALIDAR POPUP RELACIONAR TRAMITE

        relacionar = "btnViewEntityCalls"

        llamados = "//body/div/div/div/div/div/div/div/div/div/h4[1]"

        driver.find_element_by_id(relacionar).click() 

        time.sleep(6)

        assertEqual(llamados, llamados, "No despliega popup de Relacionar Tramites") 

        #EVIDENCIA DEL CP5    

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP5.png")
        time.sleep(3)

        #VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE 

        lista = "listMyTaskGrid_length"

        driver.find_element_by_name(lista).click() 

        time.sleep(10)

        #EVIDENCIA DEL CP6 y CP7//VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 10 TRAMITES

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP6yCP7.png")
        time.sleep(3 )

        #VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 25 TRAMITES

        lista25 = "//div[@role='grid']//div//div//div//option[@value='25']"

        driver.find_element_by_xpath(lista25).click() 

        time.sleep(3)

        #EVIDENCIA DEL CP8//VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 25 TRAMITES

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP8.png")
        time.sleep(3 )

        #VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 50 TRAMITES

        lista50 = "//option[@value='50']"

        driver.find_element_by_xpath(lista50).click() 

        time.sleep(3)

        #EVIDENCIA DEL CP9//VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 50 TRAMITES

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP9.png")
        time.sleep(3 )

        #VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 100 TRAMITES

        lista100 = "//option[@value='100']"

        driver.find_element_by_xpath(lista100).click() 

        time.sleep(3)

        #EVIDENCIA DEL CP10//VALIDAR LISTA DESPLEGABLE RELACIONAR TRAMITE CON 100 TRAMITES

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP10.png")
        time.sleep(3)

        #VALIDAR AVANCE DE PAG 
        
        flecha = "2"

        driver.find_element_by_link_text(flecha).click()

        time.sleep(3)

        #EVIDENCIA DEL CP12//VALIDAR FLECHA DE POP UP RELACIONAR TRAMITES

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP12.png")
        time.sleep(3 )

        #VALIDAR FILTRO DE POP UP RELACIONAR TRAMITES

        lista100 = driver.find_element_by_xpath("//input[@aria-controls='listMyTaskGrid']").send_keys("207461514")

        time.sleep(3)        

        #EVIDENCIA DEL CP11//VALIDAR FILTRO DE POP UP RELACIONAR TRAMITES

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP11.png")
        time.sleep(3 )        

        time.sleep(4)        

        #VALIDAR CIERRE DE POP UP RELACIONAR TRAMITES

        driver.find_element_by_xpath("//body/div/div/div/div/div/div/div/div/div/span/a[@href='#']/i[1]").click()

        time.sleep(3)

        #EVIDENCIA DEL CP13//VALIDAR CIERRE DEL POP UP 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\CP13.png")
        time.sleep(3 )        

        time.sleep(4)

        #VALIDAR BOTON RELACIONAR CON OTRAS ENTIDADES

        driver.find_element_by_id(relacionar).click() 

        time.sleep(6)

        



        





        















        
        #--------------ACCEDER AL PROCESO------------------------

        
        driver.find_element_by_name(observacion).send_keys("Proceso CIC")

        time.sleep(5)

            
    #------GRABAR EL PROCESO------------

        driver.find_element_by_id("btnSave").click()
        time.sleep(3)
    
    #------CAPTURA DE PANTALLA------------

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\generaciontramiteCIC.png")

        time.sleep(5)
    
    

if __name__ == '__main__':
    unittest.main()
