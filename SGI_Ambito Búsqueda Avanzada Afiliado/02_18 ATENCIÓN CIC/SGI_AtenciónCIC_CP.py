import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
import time 
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
import re
from selenium.common.exceptions import TimeoutException
import datetime
from openpyxl import Workbook
import HtmlTestRunner

"""

Script para la ejecución del Proceso Atención CIC con cada uno de los motivos de carga
y cierre del ETE en la bandeja de Resolver

"""


class Atencion_Cic(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")
        
    def test_ingresosgi(self):        
        
        #INSTRUCCIONES PARA ABRIR VENTANA SGI
        
        #URL

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"        
        
        driver = self.driver
        driver.maximize_window()
        driver.get(url)  
        titulopage = driver.title
        driver.implicitly_wait(15)       

		# verify title is SGI

        assertEqual = self.assertEqual
        assertEqual("SGI", titulopage, "No pudo acceder a SGI")    
       

        #INGRESO DE USUARIOS---ARCHIVO DE EXCEL

        usuario = "ext_roperalt"
        contrasenia = "Qactions01"
        username = "userName"
        password = "password"
        disable = "disable"

        driver.find_element_by_id(username).send_keys(usuario)         
    
        password_id = WebDriverWait(driver, 10, 0.5).until(
        EC.presence_of_element_located((By.ID, password)))
        password_id.send_keys(contrasenia)
        password_id.submit()         

        # verify el correcto ingreso a SGI
        
        home_xpath = "//a[contains(text(),'Home')]"
        ingreso_sgi = driver.find_element_by_xpath(home_xpath).text
        print(ingreso_sgi)
        assertEqual(ingreso_sgi, "Home", "No pudo acceder a SGI")    

        #BUSCAR

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        buscar = WebDriverWait(driver, 10, 0.5).until(
        EC.presence_of_element_located((By.XPATH, buscar_xpath)))
        buscar.click() 

        # verify el correcto ingreso a busqueda 

        avanzada_afiliado_xpath = "//b[contains(text(),'Búsqueda Avanzada Afiliado')]" 

        busqueda = driver.find_element_by_xpath(avanzada_afiliado_xpath).text

        assertEqual(busqueda, "Búsqueda Avanzada Afiliado", "No avanza a la busqueda de afiliado")     
        
        #INGRESO DE DU

        DU = "12270576"
        afiliado = "txtAfiliadoDocumento"
        boton = "btnSearch2"

        driver.find_element_by_id(afiliado).send_keys(DU)
        driver.find_element_by_id(boton).click()   

        # verify el correcto ingreso al afiliado

        datos_afiliado_xpath = "//a[normalize-space()='Datos Del Afiliado']"

        page_afiliado = driver.find_element_by_xpath(datos_afiliado_xpath).text
        print(page_afiliado)

        #afiliado = WebDriverWait(driver, 10, 0.5).until(
        #EC.visibility_of_element_located((By.XPATH, page_afiliado)))        

        assertEqual(page_afiliado, "Datos Del Afiliado", "No avanza a la busqueda de afiliado")        

        lista_motivo = ['ABM','COBERTURA', 'CONSTANCIA DE ATENCION MEDICA','CONSULTA DE PRESTACIONES','LEGALES','PRIORIDADES',
                        'SOPORTE WEB','TIEMPOS CUMPLIDOS PEDIDO DISCAPACIDAD','BENEFICIOS SMG','CONSTANCIA DE ATENCION MEDICA',
                        'CONSULTAS DMP', 'CONTENIDO BASE DE CONOCIMIENTO','COPAGOS', 'DISCAPACIDAD PAÑALES', 'ENCUESTA', 'EVALUACION DE CARENCIAS',
                        'GESTION CALIDAD', 'GESTION SUCURSAL', 'LEGALES', 'PRIORIDADES', 'REDES SOCIALES', 'REGALO RECIEN NACIDO',
                        'REINTEGROS CARGADOS EN SUCURSAL (NO CPR)', 'SOPORTE WEB', 'TIEMPOS CUMPLIDOS PEDIDO AUTORIZACIONES',
                        'TIEMPOS CUMPLIDOS PEDIDO FARMACIA', 'TIEMPOS CUMPLIDOS PEDIDO FERTILIDAD', 'TIEMPOS CUMPLIDOS PEDIDO PROTESIS']      
        
       
        #FORMATO DE FECHA 

        formato = "%d %m %Y %H:%M:%S"

        fecha = datetime.datetime.today()
        print('ISO     :', fecha)

        today = fecha.strftime(formato)
        print('strftime:', today)

        # ABRO ARCHIVO TXT 
            
        f = open('Tramite.txt','x')

        # FECHA DE HOY

        #f.write(today +'\n')

        # CIERRO ARCHIVO

        f.close()  
        
        for i in lista_motivo:  
            
            #MOVER HASTA LA RUEDA

            buscarueda = driver.find_element_by_css_selector("#ace-settings-btn")
            webdriver.ActionChains(driver).move_to_element(buscarueda).perform()

            #SELECCIONAR PROCESO

            #proceso = "inputProcess"
            #nombre = "CIC"
            numero = "515"
            busqueda_proceso_xpath = "//li[@title='Atención CIC']"

            proceso_assert = driver.find_element_by_xpath(busqueda_proceso_xpath).text

            busqueda_rueda_proceso = WebDriverWait(driver, 10, 0.5).until(
            EC.presence_of_element_located((By.ID,numero ))) 

            # Validación de disponibilidad del proceso "Atencion CIC" en la ruedita de "Procesos".

            assertEqual(proceso_assert,'Atención CIC', "No se encuentra el proceso")      

            driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\Atención CIC_000.png")
        
            driver.find_element_by_id(numero).click()

            buscarueda.click()   

            #------ACCEDER AL FRAME PROCESO------------

            iframe = "my_frame515" 

            WebDriverWait(driver, 10, 0.5).until(
            EC.frame_to_be_available_and_switch_to_it((By.ID, iframe)))

            #------MOVER MOUSE HASTA PANTALLA DEL PROCESO------------   

            #roceso_page = driver.find_element_by_id("515-link-tab")
            #webdriver.ActionChains(driver).drag_and_drop(proceso_page).perform()    

            #driver.find_element_by_name(observacion_name).send_keys("Proceso CIC") 

            page_proceso_xpath = "//h4[contains(text(),'Atención CIC')]"           

            page_proceso = driver.find_element_by_xpath(page_proceso_xpath).text
            print(page_proceso)            

            #Validacion de pantalla del proceso "Atencion CIC"-----Atención CIC_001

            assertEqual(page_proceso, "Atención CIC", "No se accedió a la página del Proceso") 

            #EVIDENCIA DEL CP1       

            driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\Atención CIC_001.png")
            
            #Validación de campo "Observación"          
            
            label_observacion = "//label[@for='observation']"
            campo_observacion = driver.find_element_by_xpath(label_observacion).text
            print(campo_observacion)
            
            assertEqual(campo_observacion, "Observaciones: *", "No encuentra campo de observación")       
            
            #Validación de campo "Observación" 

            #driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\Atención CIC_002.png")
        
            #Validación de campo obligatorio "Observación"del proceso                       

            # select by visible text

            motivo_cic_xpath = "atencionCIC[detalle][motivo][id]"

            motivo = Select(driver.find_element_by_name(motivo_cic_xpath))

            motivo.select_by_visible_text(i)

            time.sleep(2)

            #driver.find_element_by_xpath(i)

            #------------BOTÓN GRABAR TRÁMITE-------------------------

            grabar_id = "btnSave"

            driver.find_element_by_id(grabar_id).click()         

            mensaje_obligatoriedad_observacion_xpath = "//p[contains(text(),'Debe ingresar una observación.')]"

            mensaje = driver.find_element_by_xpath(mensaje_obligatoriedad_observacion_xpath).text

            print (mensaje)

            def esperar_xpath(self, XPATH):  # Esperar que un elemento sea visible
                try:
                    wait = WebDriverWait(self.driver, 10)
                    wait.until(EC.visibility_of_element_located((By.XPATH, mensaje_obligatoriedad_observacion_xpath)))
                    print(u"esperar_Xpath: Se mostró el elemento " + mensaje_obligatoriedad_observacion_xpath)
                    return True

                except TimeoutException:
                    self.msj = (u"esperar_Xpath: No presente " + mensaje_obligatoriedad_observacion_xpath)
                    return False 
            
            WebDriverWait(driver, 10, 0.5).until(
            EC.presence_of_element_located((By.XPATH,mensaje_obligatoriedad_observacion_xpath )))       
            
            #assertEqual(mensaje_obligatoriedad_observacion_xpath,"Debe ingresar una observación.", "No se considera campo obligatorio") 
        
            #EVIDENCIA DEL Atención CIC_003 y Atención CIC_004  

            driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC\\Evidencia_CP\\Atención CIC_003-Atención CIC_004.png")
            
         
            #AGREGAR CARACTERES EN EL CAMPO DE OBSERVACION            

            observacion_name = "atencionCIC[detalle][observacion]"

            observacion = WebDriverWait(driver, 10, 0.5).until(
            EC.presence_of_element_located((By.NAME, observacion_name)))

            observacion.send_keys("Proceso CIC")             

            #GRABAR TRAMITE

            driver.find_element_by_id(grabar_id).click() 

            #VISIBILIDAD DE BOTÓN CLIQUEABLE ACEPTAR DEL POP UP GENERACION TRAMITE

            button_aceptar_xpath = "//a[contains(text(),'Aceptar')]"

            WebDriverWait(driver, 15).until(
            EC.element_to_be_clickable((By.XPATH, button_aceptar_xpath )))
        
            #--------TOMAR TRAMITE

            #TOMAR TRÁMITE
        
            numerotramite = driver.find_element_by_xpath("//*[@class='bootbox modal fade in']/child::div[1]").text

            #//body/div[7]/#body:nth-child(2) > div.bootbox.modal.fade.in:nth-child(8)/#/html/body/div[7]/div[1]

            print(numerotramite)

            busqueda_tramite = r"(?<=Trámite )\w+"

            numero_tramite = re.findall(str(busqueda_tramite),numerotramite, re.IGNORECASE)

            print(numero_tramite)

            numero_tramite = str(numero_tramite[0])

            print(numero_tramite)

            tramite = "#" + numero_tramite
            
            print(tramite) 

            time.sleep(2)

            #--------BOTÓN ACEPTAR DE POP UP GENERACIÓN TRÁMITE

            driver.find_element_by_xpath("//a[contains(text(),'Aceptar')]").click() 

            #--------ABRIR DOCUMENTO PARA GRABAR LOS TRÁMITES

            f = open('Tramite.txt','a')
            f.write(tramite +'\n')
            f.close()
    

        archivo = open("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\Tramite.txt","r")
        
        for tramite in archivo:
        
            #--------BUSQUEDA INTELIGENTE TRAMITE        

            driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite) 

            time.sleep(2)     

            driver.find_element_by_id("advanceSearch").click()                  

            #--------INGRESO AL TRAMITE 

            #-----VALIDACIÓN DEL TRAMITE CORRECTO

            tramite_validacion = tramite[1:10]           

            tramite_bandeja = driver.find_element_by_id(tramite_validacion)            
            
            if tramite_validacion == tramite_bandeja:
               
               #icono_barra_id = "tagTimerTasks"

                filtro_bandeja_xpath = "//input[@aria-controls='listUnassignTaskGrid']"

                WebDriverWait(driver, 15).until(
                EC.visibility_of_element_located((By.XPATH, filtro_bandeja_xpath))) 
            
            else:
                
                driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite) 

                time.sleep(2)     

                driver.find_element_by_id("advanceSearch").click()

            #--------INGRESO AL TRAMITE

            lupa = driver.find_element_by_id("advanceSearch")

            icono = driver.find_element_by_id("tagDateTasks")

            webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()      

            tomar_abrir_xpath = "//a[@data-original-title='Tomar y Abrir']"
            
            abrir_tramite = WebDriverWait(driver, 15).until(
            EC.element_to_be_clickable((By.XPATH, tomar_abrir_xpath))) 

            abrir_tramite.click()             

            #------ACCEDER AL FRAME TAREAS------------

            iframe_id_task = "iframeTask" 

            WebDriverWait(driver, 10).until(
            EC.frame_to_be_available_and_switch_to_it((By.ID, iframe_id_task)))  

            #--------GESTION  Atencion CIC - Resolver

            #formulario_resolver_id = "formTabDetail"

            #page_resolver = WebDriverWait(driver, 15, 0.5).until(
            #EC.new_window_is_opened((By.ID,formulario_resolver_id))) 
            #page_resolver.click()

            #--------VALIDACIÓN DE GESTIONAR TRAMITE CORRECTO

            page_resolver_xpath = "//h5[contains(text(),'Trámite {tramite} - Atencion CIC - Resolver')]"

            validacion_page = driver.find_element_by_xpath(page_resolver_xpath).text

            print(validacion_page)

            page_tramite = r"(?<=Trámite )\w+"

            numero_tramite_page_task = re.findall(str(page_tramite),validacion_page, re.IGNORECASE)

            tramite_task = (numero_tramite_page_task[0])

            if tramite_task == tramite:

                observacion_completar_name = "atencionCIC[detalle][observacionResolver]"

                observacion_resolver = WebDriverWait(driver, 15, 0.5).until(
                EC.presence_of_element_located((By.NAME, observacion_completar_name))) 

            else:
                
                driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite) 

                time.sleep(2)     

                driver.find_element_by_id("advanceSearch").click()

                #--------INGRESO AL TRAMITE

                lupa = driver.find_element_by_id("advanceSearch")

                icono = driver.find_element_by_id("tagDateTasks")

                webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()      

                tomar_abrir_xpath = "//a[@data-original-title='Tomar y Abrir']"
                
                abrir_tramite = WebDriverWait(driver, 15).until(
                EC.element_to_be_clickable((By.XPATH, tomar_abrir_xpath))) 

                abrir_tramite.click()             

                #------ACCEDER AL FRAME TAREAS------------

                iframe_id_task = "iframeTask" 

                WebDriverWait(driver, 10).until(
                EC.frame_to_be_available_and_switch_to_it((By.ID, iframe_id_task)))  

            observacion_resolver.send_keys("Atencion CIC - Completar Informacion")   

            seleccione_accion_id = Select(driver.find_element_by_id("actions"))

            seleccione_accion_id.select_by_visible_text("Resuelta")  

            flecha = "btnSaveAndEndTask"

            driver.find_element_by_id(flecha).click()

            time.sleep(3)

            #--------BUSCAR TRAMITE 

            driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

            time.sleep(2)

            driver.find_element_by_id("advanceSearch").click()

            time.sleep(3) 

if __name__ == '__main__':
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_18 ATENCIÓN CIC'))
