import unittest
from unittest.main import main
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
#from webdriver_manager.chrome import ChromeDriverManager


class AtencionSucursal(unittest.TestCase):

    def setUp(self):        

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")
        #self.driver = webdriver.Chrome(ChromeDriverManager().install())

    def test_Atencion(self):

        #ACCESO A SGI

        driver = self.driver
        driver.maximize_window()

        url = "http://sgipre/sgi-app/#login"
        url2= "http://sgiqa/sgi-app/#login"

        driver.get(url)
        time.sleep(7)

        #VERIFICAR EL TITULO DE SGI 

        titulopage = driver.title

        assertEqual = self.assertEqual

        assertEqual = ("SGI", titulopage, "No se logró acceder a SGI")

        #DATOS DE ACCESO SGI

        usuario = "EXT_MaFernan"
        contrasena = "u2y9fXxch"
        user_id = "userName"
        password_id = "password"
        ingresar_button = "disable"

        driver.find_element_by_id(user_id).send_keys(usuario)
        driver.find_element_by_id(password_id).send_keys(contrasena)
        driver.find_element_by_id(ingresar_button).click()
        time.sleep(3)        

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)    

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]
        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[0]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA AGRADECIMIENTO

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP1.png")
        time.sleep(3) 

        #--------CP 2-------------------------------------------------

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO
       
        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[1]).click()
        time.sleep(4)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA Atención CIC

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP2.png")
        time.sleep(3) 

        #--------CP 3--------------NO APLICA-----------------------------------

        #--------CP 4-------------------------------------------------

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[2]).click()
        time.sleep(4)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA Autorizaciones CALL

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP4.png")
        time.sleep(3) 

        #--------CP 5-------------------------------------------------

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']","//option[@value='Censo Internaciones']"]
        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[3]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA COT (Salud Mental)

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP5.png")
        time.sleep(3) 

        #--------CP 6------------------------------------------------- 

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[4]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA Censo Internaciones

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP6.png")
        time.sleep(3) 

        #--------CP 7-------------------------------------------------

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[5]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA Certificado de Antigüedad

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP7.png")
        time.sleep(3) 

        #--------CP 8------------------------------------------------- 

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[6]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA Clientes VIP

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP8.png")
        time.sleep(3) 

         #--------CP 9------------------------------------------------- 

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[7]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA Clientes VIP Marcas Adicionales

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP9.png")
        time.sleep(3) 

        #--------CP 10------------NO APLICA------------------------------------- 

        #--------CP 11------------------------------------------------

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[8]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA Consulta

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP11.png")
        time.sleep(3) 

        #--------CP 12------------NO APLICA------------------------------------- 

        #--------CP 13------------------------------------------------         

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[9]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA Consulta Cartilla Farmacias

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP13.png")
        time.sleep(3) 

    #--------CP 14

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[10]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP14.png")
        time.sleep(3) 

        #--------CP 15 = NO APLICA   

        #--------CP 16

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[11]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP16.png")
        time.sleep(3) 

        #--------CP 17

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[12]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP17.png")
        time.sleep(3) 

        #--------CP 18

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[13]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP18.png")
        time.sleep(3) 

        #--------CP 19

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[14]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP19.png")
        time.sleep(3) 

        #--------CP 20

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[15]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP20.png")
        time.sleep(3) 

        #--------CP 21

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[16]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP21.png")
        time.sleep(3) 

        #--------CP 22

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[17]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP22.png")
        time.sleep(3) 

        #--------CP 23

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[18]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP23.png")
        time.sleep(3) 

        #--------CP 24------

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[19]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP24.png")
        time.sleep(3) 

        #--------CP 25------NO APLICA        

        #--------CP 26

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[20]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP26.png")
        time.sleep(3)

        #--------CP 27

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[21]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP27.png")
        time.sleep(3)

        #--------CP 28

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[22]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP28.png")
        time.sleep(3)

        #--------CP 29

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[23]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP29.png")
        time.sleep(3)

        #--------CP 30------------NO APLICA

        #--------CP 31

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[24]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP31.png")
        time.sleep(3)

        #--------CP 32

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[25]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP32.png")
        time.sleep(3)

        #--------CP 33

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[26]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP33.png")
        time.sleep(3)

        #--------CP 34

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[27]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP34.png")
        time.sleep(3)

        #--------CP 35----NO APLICA

        #--------CP 36

         #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[28]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP36.png")
        time.sleep(3)

        #--------CP 37-----NO APLICA

        #--------CP 38

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[29]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP38.png")
        time.sleep(3)

        #--------CP 39

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[30]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP39.png")
        time.sleep(3)

        #--------CP 40

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[31]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP40.png")
        time.sleep(3)

        #--------CP 41

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[32]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP41.png")
        time.sleep(3)

        #--------CP 42-----NO APLICA

        #--------CP 43

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[33]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP43.png")
        time.sleep(3)

        #--------CP 44

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[34]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP44.png")
        time.sleep(3)

        #--------CP 45/CP 46/CP 47/CP 48-----NO APLICA

        #--------CP 49

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[35]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP49.png")
        time.sleep(3)

        #--------CP 50

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[36]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP50.png")
        time.sleep(3)

        #--------CP 51-------NO APLICA

        #--------CP 52

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[37]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP52.png")
        time.sleep(3)

        #--------CP 53-------NO APLICA

        #--------CP 54

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[38]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP54.png")
        time.sleep(3)

        #--------CP 55

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[39]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP55.png")
        time.sleep(3)

        #--------CP 56/CP 57-----NO APLICA

        #--------CP 58

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[40]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP58.png")
        time.sleep(3)

        #--------CP 59

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[41]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP59.png")
        time.sleep(3)

        #--------CP 60-----NO APLICA

        #--------CP 61

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[42]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP61.png")
        time.sleep(3)

        #--------CP 62

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[43]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP62.png")
        time.sleep(3)


        #--------CP 63-----NO APLICA

        #--------CP 64

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[44]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP64.png")
        time.sleep(3)

        #--------CP 65

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[45]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP65.png")
        time.sleep(3)

        #--------CP 66

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[46]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP66.png")
        time.sleep(3)

        #--------CP 67

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[47]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP67.png")
        time.sleep(3)

        #--------CP 68------NO APLICA

        #--------CP 69

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[48]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP69.png")
        time.sleep(3)

        #--------CP 70

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[49]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP70.png")
        time.sleep(3)

        #--------CP 71

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[50]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP71.png")
        time.sleep(3)

        #--------CP 72------NO APLICA

        #--------CP 73

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[51]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP73.png")
        time.sleep(3)

        #--------CP 74 y 75

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[52]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP74.png")
        time.sleep(3)

        #--------CP 76

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Agradecimiento']","//option[@value='Atención CIC']","//option[@value='Autorizaciones CALL']","//select[@onchange='submotivosChange(this);']//option[@value='COT (Salud Mental)']",
        "//option[@value='Censo Internaciones']","//option[@value='Certificado de Antigüedad']","//option[@value='Clientes VIP']", "//option[@value='Clientes VIP Marcas Adicionales']", 
        "//option[@value='Consulta']","//option[@value='Consulta Cartilla Farmacias']","//option[@value='Consulta Cartilla Profesional']","//option[@value='Consulta Cobranzas']",
        "//option[@value='Consulta Cta Cte Retail']","//option[@value='Consulta OOSS Cliente']","//option[@value='Consulta Priorización Prestadores']","//option[@value='Consulta Redes Sociales']",
        "//option[@value='Consulta de Prestaciones']","//option[@value='Consulta de Prestador']","//option[@value='Contrataciones Medicas']","//option[@value='Contrataciones Medicas Interior']",
        "//option[@value='Cuidados Domiciliarios']","//option[@value='Depositos']","//option[@value='Derivacion Medica Programada']","//option[@value='Derivación Médica de Urgencia']",
        "//option[@value='Discapacidad']","//option[@value='Documentación SUR OOSS']","//option[@value='Entrega Regalo Recién Nacido']","//option[@value='Excepciones al Cliente']",
        "//option[@value='Gestion Autorizaciones']","//option[@value='Gestion Quejas']","//option[@value='Gestion de Aut. Odontologicas']","//option[@value='Gestión Presupuestos de Prestadores']",
        "//option[@value='Gestión de Reintegros']","//option[@value='Interconsultas']","//option[@value='Modalidad de Pago']","//option[@value='Pedido ADM Cartera']","//option[@value='Pedido Cartilla Prestadores']",
        "//option[@value='Pedido Contact Center']","//option[@value='Pedido Sucursal']","//option[@value='Pedido Temas Farmacias']","//option[@value='Plan Anual Discapacidad']","//option[@value='Prótesis (Ex PTP)']",
        "//option[@value='Reenvío de Factura']","//option[@value='Reintegros']","//option[@value='Retención de Clientes']","//option[@value='Retención de Clientes']","//option[@value='Solicitud Web']","//option[@value='Solicitud de Credenciales']",
        "//option[@value='Sugerencia']","//option[@value='Tramites En Caja']","//option[@value='Traslados Programados Control Imagen']","//option[@value='Trámite Res. 155/2018']","//option[@value='Verificacion de Datos']","//option[@value='Visita a Paciente Internado']"]        
                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='1']"
        tipo_telefono_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div[2]//div[1]//div[1]//select[1]//option[2]"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        atender_id2 = driver.find_element_by_id(atender_id)

        driver.find_element_by_xpath(motivo_xpath[0]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        time.sleep(2)
        driver.find_element_by_xpath(tipo_telefono_xpath).click()
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP76-1.png")
        time.sleep(5)

        action = webdriver.ActionChains(driver)

        action.move_to_element(atender_id2)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP76-2.png")
        time.sleep(5)        

        #--------CP 77

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = "//option[@value='Agradecimiento']"                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='1']"
        tipo_telefono_xpath = "//option[contains(@value,'(011) 51232345')]"
        atender_id = "chkAttentionNow"
        atender_id2 = driver.find_element_by_id(atender_id)
      

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        time.sleep(2)
        driver.find_element_by_xpath(tipo_telefono_xpath).click()
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP77-1.png")
        time.sleep(3)

        action = webdriver.ActionChains(driver)

        action.move_to_element(atender_id2)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP77-2.png")
        time.sleep(3)

        
        #--------CP 78

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

         #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = "//option[@value='Agradecimiento']"

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='1']"
        tipo_telefono_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/select[1]/option[5]"
        otroid = "otroTelContacto"       
        atender_id = "chkAttentionNow"
        atender_id2 = driver.find_element_by_id(atender_id)
        

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        time.sleep(5)
        driver.find_element_by_xpath(tipo_telefono_xpath).click()
        time.sleep(5)
        driver.find_element_by_id(otroid).send_keys("(011) 51232345")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP78-1.png")
        time.sleep(3)

        action = webdriver.ActionChains(driver)

        action.move_to_element(atender_id2)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP78-2.png")
        time.sleep(3)

        #--------CP 79------NO APLICA

        #--------CP 80

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

       #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = "//option[@value='Agradecimiento']"
        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        tipo_mail_xpath = "//option[@value='otro']"
        otro_id = "otroMailContacto"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        atender_id2 = driver.find_element_by_id(atender_id)

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(3)
        driver.find_element_by_id(ejecutivo_id).click() 
        time.sleep(3)             
        driver.find_element_by_xpath(tipo_mail_xpath).click()
        time.sleep(3)
        driver.find_element_by_id(otro_id).send_keys("prueba@gmail.com")
        time.sleep(3)
        driver.find_element_by_id(atender_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP80-1.png")
        time.sleep(3)

        action = webdriver.ActionChains(driver)

        action.move_to_element(atender_id2)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP80-2.png")
        time.sleep(3)

        #--------CP 81

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = "//option[@value='Agradecimiento']"

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='12']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        atender_id2 = driver.find_element_by_id(atender_id)

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        time.sleep(2)        
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP81-1.png")
        time.sleep(3)

        action = webdriver.ActionChains(driver)

        action.move_to_element(atender_id2)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP81-2.png")
        time.sleep(3)

        #--------CP 82

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = "//option[@value='Agradecimiento']"

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='9']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        atender_id2 = driver.find_element_by_id(atender_id)

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        time.sleep(2)        
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP82-1.png")
        time.sleep(3)

        action = webdriver.ActionChains(driver)

        action.move_to_element(atender_id2)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP82-2.png")
        time.sleep(3)      
        
        #--------CP 86 y 87 TODOS LOS CP SON REALIZADOS DE ESTE MODO

        #--------CP 88 NO APLICA

        #--------CP 89

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Afiliados Desregulados']","//option[@value='Alta de Turno Programado/Virtual']","//option[@value='Atención Auditoría de Reintegros']",
        "//option[@value='Consulta Cobertura Medicamentos']","//option[@value='Consulta Ensayo Vacuna']","//option[@value='Solicitud Envio Documentacion']",
        "//option[@value='Solicitud de Token para Credencial']"]                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[0]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP89.png")
        time.sleep(3)

        #--------CP 90

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Afiliados Desregulados']","//option[@value='Alta de Turno Programado/Virtual']","//option[@value='Atención Auditoría de Reintegros']",
        "//option[@value='Consulta Cobertura Medicamentos']","//option[@value='Consulta Ensayo Vacuna']","//option[@value='Solicitud Envio Documentacion']",
        "//option[@value='Solicitud de Token para Credencial']"]                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[1]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP90.png")
        time.sleep(3)

        #--------CP 91

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Afiliados Desregulados']","//option[@value='Alta de Turno Programado/Virtual']","//option[@value='Atención Auditoría de Reintegros']",
        "//option[@value='Consulta Cobertura Medicamentos']","//option[@value='Consulta Ensayo Vacuna']","//option[@value='Solicitud Envio Documentacion']",
        "//option[@value='Solicitud de Token para Credencial']"]                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[2]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP91.png")
        time.sleep(3)

        #--------CP 92

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Afiliados Desregulados']","//option[@value='Alta de Turno Programado/Virtual']","//option[@value='Atención Auditoría de Reintegros']",
        "//option[@value='Consulta Cobertura Medicamentos']","//option[@value='Consulta Ensayo Vacuna']","//option[@value='Solicitud Envio Documentacion']",
        "//option[@value='Solicitud de Token para Credencial']"]                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[3]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP92.png")
        time.sleep(3)

        #--------CP 93

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Afiliados Desregulados']","//option[@value='Alta de Turno Programado/Virtual']","//option[@value='Atención Auditoría de Reintegros']",
        "//option[@value='Consulta Cobertura Medicamentos']","//option[@value='Consulta Ensayo Vacuna']","//option[@value='Solicitud Envio Documentacion']",
        "//option[@value='Solicitud de Token para Credencial']"]                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[4]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP93.png")
        time.sleep(3)

        #--------CP 94------NO APLICA

        #--------CP 95

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Afiliados Desregulados']","//option[@value='Alta de Turno Programado/Virtual']","//option[@value='Atención Auditoría de Reintegros']",
        "//option[@value='Consulta Cobertura Medicamentos']","//option[@value='Consulta Ensayo Vacuna']","//option[@value='Solicitud Envio Documentacion']",
        "//option[@value='Solicitud de Token para Credencial']"]                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[5]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP95.png")
        time.sleep(3)       

        #--------CP 96

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = ["//option[@value='Afiliados Desregulados']","//option[@value='Alta de Turno Programado/Virtual']","//option[@value='Atención Auditoría de Reintegros']",
        "//option[@value='Consulta Cobertura Medicamentos']","//option[@value='Consulta Ensayo Vacuna']","//option[@value='Solicitud Envio Documentacion']",
        "//option[@value='Solicitud de Token para Credencial']"]                      

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='4']"
        observacion_id = "obs-motivo-recepcion"
        atender_id = "chkAttentionNow"
        aceptar_button_id = "btnAceptarRecepcion"

        driver.find_element_by_xpath(motivo_xpath[6]).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()
        driver.find_element_by_id(observacion_id).send_keys("Prueba")
        time.sleep(2)
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP96.png")
        time.sleep(3)       

        #--------CP 97       

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "7651917"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = "//option[@value='Alta de Turno Programado/Virtual']"        

        ejecutivo_id = "CN=Matias Fernandez Oro,OU=Externos,OU=Groups&Users,OU=-SMG-,DC=swm,DC=com,DC=ar"
        contacto_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='20']"
        atender_id = "chkAttentionNow"
        atender_id2 = driver.find_element_by_id(atender_id)

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(2)
        driver.find_element_by_id(ejecutivo_id).click()
        driver.find_element_by_xpath(contacto_xpath).click()        
        driver.find_element_by_id(atender_id).click()
        time.sleep(2)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP97-1.png")
        time.sleep(3)

        action = webdriver.ActionChains(driver)

        action.move_to_element(atender_id2)

        time.sleep(2)

        action.key_down(Keys.TAB)

        action.key_up(Keys.TAB)

        action.key_down(Keys.ENTER)

        action.key_up(Keys.ENTER)

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP97-2.png")
        time.sleep(3)

        #--------CP 98

        #ACCEDER A PAG DE BUSQUEDA DE AFILIADO

        buscar_xpath = "//a[@id='searchTypeEntity']//span[@class='menu-text']"

        driver.find_element_by_xpath(buscar_xpath).click()
        time.sleep(4)

        #INGRESAR DU 

        documento = "45822651"
        du_id = "txtAfiliadoDocumento"
        buscar_button_id = "btnSearch2"

        driver.find_element_by_id(du_id).send_keys(documento)
        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(4)

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"   

        action = webdriver.ActionChains(driver)

        action.click_and_hold(recepcionar_xpath)        

        action.perform()

        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP98.png")
        time.sleep(3)        

        #--------CP 99

        #ACCEDER ATENCION SUCURSAL 

        recepcionar_xpath = "//body/div[@id='app']/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]/i[1]"

        driver.find_element_by_xpath(recepcionar_xpath).click()
        time.sleep(3)

        #SELECCIONAR OPCIÓN EN MOTIVO

        motivo_xpath = "//select[@onchange='submotivosChange(this);']"    

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(2)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP99.png")
        time.sleep(3)

        #--------CP 100

        ejecutivo_id = "ejecutivos"

        driver.find_element_by_id(ejecutivo_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP100.png")
        time.sleep(5)

        #--------CP 101

        contacto_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div//div[4]//div[1]//div[1]//div[1]//div[1]//select[1]"

        driver.find_element_by_xpath(contacto_xpath).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP101.png")
        time.sleep(5)

        #--------CP 102

        observacion_id = "obs-motivo-recepcion"

        driver.find_element_by_id(observacion_id).send_keys("Prueba123456789!?¡?¡?¡?¡")
        time.sleep(5)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP102.png")
        time.sleep(3)

        #--------CP 103

        socio_id = "chkPriority"

        driver.find_element_by_id(socio_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP103.png")
        time.sleep(3)

        #--------CP 104

        atender_id = "chkAttentionNow"

        driver.find_element_by_id(atender_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP104.png")
        time.sleep(3)

        #--------CP 105/106

        telefono_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='1']"
        tipo_telefono_id = "telContacto"

        driver.find_element_by_xpath(telefono_xpath).click()
        time.sleep(3)

        driver.find_element_by_id(tipo_telefono_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP105-106.png")
        time.sleep(3)

        #--------CP 107/108

        mail_xpath = "//body//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//select//option[@value='3']"
        tipo_telefono_id = "mailContacto"

        driver.find_element_by_xpath(mail_xpath).click()
        time.sleep(3)

        driver.find_element_by_id(tipo_telefono_id).click()
        time.sleep(3)

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP107.png")
        time.sleep(3)

        #--------CP 110

        cerrar_xpath = "//div//div//div//div//div//div[4]//div[1]//div[1]//div[1]//div[1]//span[1]//a[1]//i[1]"
        
        driver.find_element_by_xpath(cerrar_xpath).click()
        time.sleep(3)        

        #EVIDENCIA 

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_24 ATENCIÓN SUCURSAL\\Evidencia_CP\\CP110.png")
        time.sleep(3) 


if __name__ == '__main__':
    unittest.main()
    



