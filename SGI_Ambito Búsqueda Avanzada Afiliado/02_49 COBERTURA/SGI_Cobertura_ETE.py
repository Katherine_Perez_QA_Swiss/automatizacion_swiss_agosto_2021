import unittest
from unittest.main import main
from selenium import webdriver
import time

class Cobertura(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")

    def test_Cobertura(self):
        
       #INGRESO A SGI

       url = "http://sgipre/sgi-app/#login"
       url2 = "http://sgiqa/sgi-app/#login"

       driver = self.driver

       driver.maximize_window()
       driver.get(url)
       time.sleep(4)
       
       #INGRESAR CON USUARIO

       usuario = "ext_roperalt"
       contrasena="Qactions01"
       user_id = "userName"
       password_id = "password"
       ingresar_button_id = "disable"

       driver.find_element_by_id(user_id).send_keys(usuario)
       driver.find_element_by_id(password_id).send_keys(contrasena)
       driver.find_element_by_id(ingresar_button_id).click()
       time.sleep(6)

       #BUSCAR 

       driver.find_element_by_id("searchTypeEntity").click()
       time.sleep(8)
       
       #INGRESO DATOS DU

       documento = "29197733"
       du_id = "txtAfiliadoDocumento"
       buscar_button_id = "btnSearch2"

       driver.find_element_by_id(du_id).send_keys(documento)
       time.sleep(2)
       driver.find_element_by_id(buscar_button_id).click()
       time.sleep(3)

       #MOVER A LA RUEDA

       lupa_id = "ace-search-container"
       
       mover = driver.find_element_by_id(lupa_id)
       
       webdriver.ActionChains(driver).click_and_hold(mover).perform()
       
       time.sleep(5)
       
       #SELECCIONAR PROCESO

       consulta_id = "inputConsultation"
       consulta = "Cober"
       numero_consulta_id = "202"
       
       driver.find_element_by_id(consulta_id).send_keys(consulta)
       time.sleep(2)
       driver.find_element_by_id(numero_consulta_id).click()
       time.sleep(10)         
       
       #DESCARGAR COBERTURA SOCIO

       flecha_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]/i[1]"
       descargar_xpath = "//a[@title='Descargar']//i"

       driver.find_element_by_xpath(flecha_xpath).click()
       driver.find_element_by_xpath(descargar_xpath).click()
       time.sleep(2)

       #CAPTURA DE PANTALLA 

       driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_49 COBERTURA\\Evidencia_ETE\\coberturasocio.png")
       time.sleep(2)

if __name__ == '__main__':
    unittest.main()
    









    
