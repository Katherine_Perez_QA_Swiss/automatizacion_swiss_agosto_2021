import unittest
from unittest.main import main
from selenium import webdriver
import time
import autoit
from selenium.webdriver.common.keys import Keys
import re

class DerivacionMedica (unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path=r"C:\Users\QA-User61\chromedriver.exe")
    
    def test_Derivacion(self):
        
        #ACCESO A SGI 

        driver = self.driver

        url = "http://sgipre/sgi-app/#login"
        url2 = "http://sgiqa/sgi-app/#login"

        driver.maximize_window()
        driver.get(url2)
        time.sleep(15)

        #DATOS DE USUARIO 

        usuario = "EXT_MaFernan"
        contrasena = "u2y9fXxch"
        username_id = "userName"
        password_id = "password"
        disable_id = "disable"

        driver.find_element_by_id(username_id).send_keys(usuario)
        driver.find_element_by_id(password_id).send_keys(contrasena)
        driver.find_element_by_id(disable_id).click()

        time.sleep(9)

        #BUSCAR

        buscar_id = "searchTypeEntity"

        driver.find_element_by_id(buscar_id).click()
        time.sleep(5)

        #INGRESO DE DU

        DU = "29197733"
        afiliado = "txtAfiliadoDocumento"
        boton = "btnSearch2"

        driver.find_element_by_id(afiliado).send_keys(DU)
        driver.find_element_by_id(boton).click()

        time.sleep(15)

        #MOVER HASTA LA RUEDA

        rueda_ccsselector = "#ace-settings-btn"

        buscarueda = driver.find_element_by_css_selector(rueda_ccsselector)
        webdriver.ActionChains(driver).click_and_hold(buscarueda).perform()
        time.sleep(4)

        #SELECCIONAR UN PROCESO

        proceso_id = "inputProcess"
        nombre = "Deri"
        numero_id = "109"
        
        driver.find_element_by_id(proceso_id).send_keys(nombre)
        time.sleep(2)
        driver.find_element_by_id(numero_id).click()
        time.sleep(3)

        #ACCEDER AL IFRAME PROCESO

        iframe = "my_frame109" 

        driver.switch_to.frame(driver.find_element_by_id(iframe))
        time.sleep(5) 

        #GESTION DE PROCESO

        #SWICHT ¿Se verificó la existencia de otro prestador?

        prestador_id = "otroPrestador"

        driver.find_element_by_id(prestador_id).click()

        #SWICHT ¿Se controló el cuadro de convenio de OOSS?

        convenio_id = "convenioOOSS"

        driver.find_element_by_id(convenio_id).click()
        time.sleep(4)

        #LISTA DESPLEGABLE : ¿Cuál es el motivo de ingreso?

        motivo_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/select/option[2]"

        driver.find_element_by_xpath(motivo_xpath).click()
        time.sleep(5)

        #LISTA DESPLEGABLE : Tipo de Servicio----OPCION ATENCION DEMANDA

        servicio_xpath = "//div//div//div//div//div//div//div//div//form[@method='get']//div//div//div//div//div//option[@value='8']"
        
        driver.find_element_by_xpath(servicio_xpath).click()
        time.sleep(5)

        #LISTA DESPLEGABLE : Especialidad

        especialidad_id = "6"
        driver.find_element_by_id(especialidad_id).click()
        time.sleep(5)

        #LUPA DIAGNOSTICO

        lupa_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//div//a[1]//i[1]"
        driver.find_element_by_xpath(lupa_xpath).click()
        time.sleep(2)

        #POP UP BUSQUEDA AVANZADA DIAGNOSTICOS---CODIGO

        codigo_id = "icdCodigoSearchsolicitudDMP"

        driver.find_element_by_id(codigo_id).send_keys("R05")

        #POP UP BUSQUEDA AVANZADA DIAGNOSTICOS---BUSCAR

        buscar_button_id = "solicitudDMPbtnSearch"

        driver.find_element_by_id(buscar_button_id).click()
        time.sleep(5)

        #Presenta documentación-----Resumen de historia clínica

        resumen_id = "documento-0"

        driver.find_element_by_id(resumen_id).click()

        time.sleep(3)

        #ADJUNTAR DOCUMENTO

        adjuntar_xpath = "//i[@data-rel='tooltip']"

        driver.find_element_by_xpath(adjuntar_xpath).click()
        time.sleep(2)

        cargararchivo = "btnClickCargarArchivosRUDI"

        driver.find_element_by_id(cargararchivo).click()

        time.sleep(4)

        autoit.win_wait("Abrir",5)

        autoit.control_focus("Abrir", "Edit1")

        autoit.win_wait("Abrir",5)

        autoit.control_set_text("Abrir","Edit1","C:\\Users\\QA-User61\\Desktop\\maty")

        autoit.win_wait("Abrir",5)

        autoit.control_click("Abrir","Button1")

        time.sleep(10)

        action = webdriver.ActionChains(driver)

        action.key_down(Keys.TAB)

        time.sleep(1) 

        action.key_up(Keys.TAB)

        time.sleep(1) 

        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)

        #SIGUIENTE A PASO 2    

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #PASO 2  

        excepcion_id = "excepcionar"

        driver.find_element_by_id(excepcion_id).click()
        time.sleep(2)

        #CONTNUAR AL PASO 3

        siguiente_id = "btnContinuar"

        driver.find_element_by_id(siguiente_id).click()
        time.sleep(2)

        #PASO 3

        #Nombre del Contacto

        nombre_name = "solicitudDMP[contacto][nombre]"

        driver.find_element_by_name(nombre_name).send_keys("Prueba")
        time.sleep(3)

        #Teléfono del Contacto

        telefono_button_id = "cargarNuevoTelefonoContacto"

        driver.find_element_by_id(telefono_button_id).click()
        time.sleep(5)

        #POP UP Nuevo Teléfono---PROVINCIA

        provincia_xpath = "//body/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div/div/select/option[2]"

        driver.find_element_by_xpath(provincia_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---PARTIDO

        partido_xpath = "//body/div/div/div/div/div/div/div/form[@method='get']/div/div/div/div/div/div/div/div[2]/div[1]/select[1]/option[2]"

        driver.find_element_by_xpath(partido_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---LOCALIDAD

        localidad_xpath = "//div//div//div//div//div//div//div//div//div//div//div//div[3]//div[1]//select[1]//option[2]"

        driver.find_element_by_xpath(localidad_xpath).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---NUMERO

        numero_id2= "prestadorPhonenumero"

        driver.find_element_by_id(numero_id2).send_keys("1123938926")
        time.sleep(2)

        #POP UP Nuevo Teléfono---VALIDAR

        validar_id = "prestadorPhonevalidatePhoneButton"

        driver.find_element_by_id(validar_id).click()
        time.sleep(2)

        #POP UP Nuevo Teléfono---CONFIRMAR        
        
        action = webdriver.ActionChains(driver)
        
        action.key_down(Keys.ENTER)

        action.perform()

        time.sleep(3)        

        #FINALIZAR TRAMITE

        finalizar_id = "btnContinuar"

        driver.find_element_by_id(finalizar_id).click()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_3\\ETE\\Generacion_Tramite.png")
        time.sleep(5)  

       #TOMAR TRÁMITE
    
        numerotramite = driver.find_element_by_xpath("//*[@class='bootbox modal fade in']/child::div[1]").text

        #//body/div[7]/#body:nth-child(2) > div.bootbox.modal.fade.in:nth-child(8)/#/html/body/div[7]/div[1]

        print(numerotramite)

        busqueda_tramite = r"(?<=Trámite )\w+"

        numero_tramite = re.findall(str(busqueda_tramite),numerotramite, re.IGNORECASE)

        print(numero_tramite) 

        tramite = "#" + str(numero_tramite[0])
        
        print(tramite) 

        driver.find_element_by_xpath("//body/div/div/a[1]").click()

        time.sleep(4)       

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_3\\01-Contactar_Empresa\\ContactarEmpresa.png")
        time.sleep(5) 

        #ABRIR TRAMITE

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(8)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #--------- Derivacion Medica Programada - Aprobar Excepcion

        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Contactar Empresa")

        time.sleep(3)

        #--------- Derivacion Medica Programada - CONTINUAR

        continuar_id = "1"

        driver.find_element_by_id(continuar_id).click()

        time.sleep(2)

        #--------- Derivacion Medica Programada - FLECHA

        flecha_xpath = "//body/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/form[@method='get']/div/div/div/span/i[1]"

        driver.find_element_by_xpath(flecha_xpath).click()

        time.sleep(8)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_3\\02-Comunicar_Socio\\ComunicarSocio_3.png")
        time.sleep(5) 

        #ABRIR TRAMITE

        driver.find_element_by_xpath("//i[@class='icon-only icon-folder-open']").click()
        time.sleep(3)

        #--------INGRESAR IFRAME TASK

        driver.switch_to_frame(driver.find_element_by_id("iframeTask"))

        time.sleep(3)

        #Validación de pantalla de la tarea "Comunicar socio"----Com soc - 004

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_3\\02-Comunicar_Socio\\ComunicarSocio_4.png")
        time.sleep(5)   

        #Validación del switch "Se comunico con el socio"----Com soc - 005

        switch_comunico_socio_id = "seComunicoConSocio"

        driver.find_element_by_id(switch_comunico_socio_id).click()

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_3\\02-Comunicar_Socio\\ComunicarSocio_5.png")
        time.sleep(5) 

        #Validación de ingreso de caracteres en el campo observación-----Com soc - 006
                
        observacion_id = "observacion"

        driver.find_element_by_id(observacion_id).send_keys("Derivacion Medica Programada - Contactar Socio123456+{+{+}")

        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_3\\02-Comunicar_Socio\\ComunicarSocio_6.png")
        time.sleep(5) 

        #Validación del campo tipo combo "Seleccione una acción"-----Com soc - 007

        accion_id = "1"        

        driver.find_element_by_id(accion_id).click()
        time.sleep(2)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_3\\02-Comunicar_Socio\\ComunicarSocio_7.png")
        time.sleep(5) 

        #FINALIZAR TRAMITE 

        finalizar_id = "1"

        driver.find_element_by_id(finalizar_id).click()

        time.sleep(2)     

       
        #DERIVAR CONTACTAR SOCIO---FLECHA

        flecha_id = "btnSaveAndEndTask"

        driver.find_element_by_id(flecha_id).click()

        time.sleep(5)

        #--------BUSQUEDA INTELIGENTE TRAMITE

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(5)

        driver.find_element_by_id("inputAdvanceSearch").send_keys(tramite)

        time.sleep(2)

        driver.find_element_by_id("advanceSearch").click()

        time.sleep(8)

        #--------INGRESO AL TRAMITE

        lupa = driver.find_element_by_id("advanceSearch")

        icono = driver.find_element_by_css_selector("i[class='fa fa-user']")

        webdriver.ActionChains(driver).drag_and_drop(lupa,icono).perform()
        time.sleep(3)

        #CAPTURE PANTALLA

        driver.get_screenshot_as_file("C:\\Users\\QA-User61\\Documents\\SGI_Ambito Búsqueda Avanzada Afiliado\\02_29 DERIVACIÓN MEDICA PROGRAMADA\\Evidencia_CP\\CP_3\\ETE\\Cierretramite.png")
        time.sleep(5)


if __name__ == '__main__':
    unittest.main()
    







































        



